import { UploadApiResult } from './model/uploadModel';
import { defHttp } from '/@/utils/http/axios';
import { UploadFileParams } from '/#/axios';
// import {id} from 'D:\TransRetail\vms-frontend-v2\src\views\components\DetailTemplates\src\inv\invUploadModal.vue';

enum Api {
  UPLOAD = 'http://localhost:3000/vmsdev/bpfr/uploadFile',
  UPLOAD_TAX_INV = 'http://localhost:3000/vmsdev/i/uploadTaxInv',
  UPLOAD_INV = 'http://localhost:3000/vmsdev/i/uploadInv',
  UPLOAD_KWI = 'http://localhost:3000/vmsdev/i/uploadKwitansi',
  UPLOAD_SJ = 'http://localhost:3000/vmsdev/i/uploadSuratJalan',
}
/**
 * @description: Upload interface
 */

export function uploadTaxInvApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  params.name = 'namafile';
  params.data = {'id':76330044}
  // console.log(Api.UPLOAD_TAX_INV)

  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.UPLOAD_TAX_INV,
      onUploadProgress,
    },
    params,
  );
}
export function uploadApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  params.name = 'namafile';
  // console.log(Api.UPLOAD)

  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.UPLOAD,
      onUploadProgress,
    },
    params,
  );
}
// let id = 76330044;
export function uploadInvApi(
  params: UploadFileParams,
  // onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  params.name = 'namafile';

  // console.log(params)

  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.UPLOAD_INV,
      // onUploadProgress,
    },
    params,
  );
}
export function uploadKwiApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  params.name = 'namafile';
  params.data = {'id':76330044}
  // console.log(Api.UPLOAD_KWI)
  // console.log(params)

  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.UPLOAD_KWI,
      onUploadProgress,
    },
    params,
  );
}
// console.log("ini apa ya", );
export function uploadSJApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: ProgressEvent) => void,
) {
  params.name = 'namafile';
  params.data = {'id':76330044}
  console.log(Api.UPLOAD_SJ)

  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.UPLOAD_SJ,
      onUploadProgress,
    },
    params,
  );
}
