import { defHttp } from '/@/utils/http/axios';
import { DemoParams, DemoListGetResultModel } from './model/tableModel';

enum Api {
  DEMO_LIST = '/table/getDemoList',
  // DEMO_ARCHIVE_DOCUMENT_LIST = '/table/getDemoArchiveDocList'
}

/**
 * @description: Get sample list value
 */

export const demoListApi = (params: DemoParams) =>
  defHttp.get<DemoListGetResultModel>({
    url: Api.DEMO_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rarItemListMockApi = (params: DemoParams) => {
  const itemList: any = {
    items: demoList,
    total: 200,
  };
  return itemList;
};

const demoList = (() => {
  const result: any[] = [];
  for (let index = 0; index < 199; index++) {
    result.push({
      reference: 'TRI1603238761026',
      id: `${index}`,
      beginTime: '@datetime',
      endTime: '@datetime',
      address: '@city()',
      merchant: 'DOMBA KECIL',
      // TRI1603238761026	DOMBA KECIL, CV	916021719	Wed, Mar 23,2016 15:27	Pembayaran sedang berjalan	Tue, Mar 29,2016 09:38	021 - Ambassador
      name: 'tisnanda',
      name1: '@cname()',
      name2: '@cname()',
      name3: '@cname()',
      name4: '@cname()',
      name5: '@cname()',
      name6: '@cname()',
      name7: '@cname()',
      name8: '@cname()',
      radio1: `选项${index + 1}`,
      radio2: `选项${index + 1}`,
      radio3: `选项${index + 1}`,
      date: `@date('yyyy-MM-dd')`,
      time: `@time('HH:mm')`,
      'no|100000-10000000': 100000,
      'status|1': ['normal', 'enable', 'disable'],
      itemCode: '22000001',
      itemCodeSub: '001',
      itemCodeUnit: '01',
      capacityAndBarcode: '1KGx1 / 2007330000000',
      itemName: 'CARICA FLOWER',
      itemNameSub: 'BUNGA PEPAYA',
      orderQuantity: 2.0,
      orderQuantityNormal: 'N',
      orderQuantityFree: 0.0,
      qtyPerPack: 2.0,
      totalQty: 2.0,
      price: 70001.0,
      matchedPrice: 80000.0,
      freeQty: 0.0,
      orderedQty: 2.0,
      isAccepted: [true, false][index % 2],
      // contentQty:
      acceptedQty: 0.0,
      revised: 'N',
      serviceLevel: 0.0,
      description: 'TEST KETERANGAN',
      raId: '1116016047',
      priceBeforeTax: 140002.0,
      priceAfterTax: 154002.2,
      taxPercentage: 10.0,
      luxuryTax: 0.0,
    });
  }
  return result;
})();
//   defHttp.get<DemoListGetResultModel>({
//     url: Api.DEMO_LIST,
//     params,
//     headers: {
//       // @ts-ignore
//       ignoreCancelToken: true,
//     },
//   });

// export const getDemoArchiveDocList = (params: DemoParams) =>
//   defHttp.get<getDemoArchiveDocListGetResultModel>({
//     url. Api.
//   })
