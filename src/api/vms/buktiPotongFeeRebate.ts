import { defHttp } from '/@/utils/http/axios';
import { LbpfrItemListModel, LbpfrParams } from './model/bpfr/viewBuktiPotongFeeRebate';
import { UbpfrItemListModel, UbpfrParams } from './model/bpfr/updateBuktiPotongFeeRebate';

enum Api {
  LBPFR_LIST = '/bpfr/getBpfrList',
  UBPFR_LIST = '/bpfr/getUnggahBpfrList',
	BPFR_DOWNLOAD = '/bpfr/getBpfrDownload',
}

/**
 * @description: Get sample list value
 */

export const lBPFRListApi = (params: LbpfrParams) =>
  defHttp.get<LbpfrItemListModel>({
    url: Api.LBPFR_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const uBPFRListApi = (params: UbpfrParams) =>
  defHttp.get<UbpfrItemListModel>({
    url: Api.UBPFR_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const BPFRDownloadApi = (params) => {
	return defHttp.get({
		url: Api.BPFR_DOWNLOAD,
		params,
		timeout: 50000,
	});
}