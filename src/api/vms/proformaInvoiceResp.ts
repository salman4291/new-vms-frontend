import { defHttp } from '/@/utils/http/axios';
import { PfirListGetResultModel, PfirParams } from './model/pfir/pfirListModel';
import { PfirItemListModel } from './model/pfir/pfirItemListModel';
import { PfirDetailInfoGetResultModel } from './model/pfir/pfirDetailInfoModel';
import { PfirActionParams, PfirActionResultModel } from './model/pfir/pfirActionModel';
import { PfirSaveParams, PfirSaveResultModel } from './model/pfir/pfirSaveModel';
import {
  PfirAcceptRejectParams,
  PfirAcceptRejectResultModel,
} from './model/pfir/pfirAcceptRejectModel';

enum Api {
  PFIR_LIST = '/pfir/getPfirAll',
  PFIR_DETAIL = `/pfir/viewbycdtdetail`,
  PFIR_INFO = '/pfir/viewbycdtinfo',
  PFIR_ACTION = '/pfir/actionPfir',
  PFIR_ACC_REJ = '/pfir/actionAcceptReject',
  PFIR_SAVE = '/pfir/actionSave',
}

/**
 * @description: Get sample list value
 */

export const pfirListApi = (params: PfirParams) =>
  defHttp.get<PfirListGetResultModel>({
    url: Api.PFIR_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const pfirDetailApi = (params) =>
  defHttp.post<PfirItemListModel>({
    url: Api.PFIR_DETAIL,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const pfirInfoApi = (params) =>
  defHttp.post<PfirDetailInfoGetResultModel>({
    url: Api.PFIR_INFO,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const pfirActionApi = (params: PfirActionParams) =>
  defHttp.post<PfirActionResultModel>({
    url: Api.PFIR_ACTION,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const pfirSaveApi = (params: PfirSaveParams) =>
  defHttp.post<PfirSaveResultModel>({
    url: Api.PFIR_SAVE,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const pfiractionAcceptRejectApi = (params: PfirAcceptRejectParams) =>
  defHttp.post<PfirAcceptRejectResultModel>({
    url: Api.PFIR_ACC_REJ,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
