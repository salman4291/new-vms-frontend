import { defHttp } from '/@/utils/http/axios';
import { RrcParams, RrcListGetResultModel } from './model/rrc/rrcListModel';
import { RrcDetailInfoGetResultModel } from './model/rrc/rrcDetailInfoModel';
import { RrcItemListModel } from './model/rrc/rrcItemListModel';

enum Api {
  RRC_LIST = '/rc/getReturnReqList',
  RRC_DETAIL = '/rc/viewbycdtdetail',
  RRC_INFO = '/rc/viewbycdtinfo',
}

/**
 * @description: Get sample list value
 */

export const rrcListApi = (params: RrcParams) =>
  defHttp.get<RrcListGetResultModel>({
    url: Api.RRC_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rrcDetailListApi = (params) =>
  defHttp.post<RrcDetailInfoGetResultModel>({
    url: Api.RRC_DETAIL,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rrcInfoListApi = (params) =>
  defHttp.post<RrcItemListModel>({
    url: Api.RRC_INFO,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });
