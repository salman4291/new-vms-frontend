import { CpfiAcceptParams, CpfiAcceptResultModel } from './model/cpfi/cpfiAcceptModel';
import { CPfiDetailInfoGetResultModel } from './model/cpfi/cpfiDetailInfoModel';
import { CPfiItemListModel } from './model/cpfi/cpfiItemListModel';
import { CPfiListGetResultModel, CPfiParams } from './model/cpfi/cpfiListModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  CPFI_LIST = '/cp/getConsPfList',
  CPFI_DETAIL = `/cp/viewbycdtdetail`,
  CPFI_INFO = '/cp/viewbycdtinfo',
  CPFI_ACTION = '/pfi/actionPfi',
  CPFI_ACCEPT = '/cp/acceptConsPfi',
  CPFI_REVERT = '/pfi/actionBack',
}

/**
 * @description: Get sample list value
 */

export const cpfiListApi = (params: CPfiParams) =>
  defHttp.get<CPfiListGetResultModel>({
    url: Api.CPFI_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const cpfiDetailApi = (params) =>
  defHttp.post<CPfiItemListModel>({
    url: Api.CPFI_DETAIL,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { cdt: params },
  });

export const cpfiInfoApi = (params) =>
  defHttp.post<CPfiDetailInfoGetResultModel>({
    url: Api.CPFI_INFO,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { cdt: params },
  });

  export const cpfiactionAcceptApi = (params: CpfiAcceptParams) =>
  defHttp.post<CpfiAcceptResultModel>({
    url: Api.CPFI_ACCEPT,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
