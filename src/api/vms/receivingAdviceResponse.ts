import { defHttp } from '/@/utils/http/axios';
import { RarParams, RarListGetResultModel } from './model/rar/rarListModel';
import { RarItemListModel } from './model/rar/rarItemListModel';
import { RarDetailInfoGetResultModel } from './model/rar/rarDetailInfoModel';
// import { RaActionParams, RaActionResultModel } from './model/rar/RarActionModel';

enum Api {
  RAR_LIST = '/rar/getRarAll',
  RAR_DETAIL = `/rar/viewbycdtdetail`,
  RAR_INFO = '/rar/viewbycdtinfo',
  RAR_ACTION = '/rar/actionRar',
}

/**
 * @description: Get sample list value
 */

export const rarListApi = (params: RarParams) =>
  defHttp.get<RarListGetResultModel>({
    url: Api.RAR_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rarDetailApi = (params) =>
  defHttp.post<RarItemListModel>({
    url: Api.RAR_DETAIL,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rarInfoApi = (params) =>
  defHttp.post<RarDetailInfoGetResultModel>({
    url: Api.RAR_INFO,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

// export const rarActionApi = (params: RaActionParams) =>
//   defHttp.post<RaActionResultModel>({
//     url: Api.RA_ACTION,
//     headers: {
//       // @ts-ignore
//       ignoreCancelToken: true,
//     },
//     data: params,
//   });
