import { defHttp } from '/@/utils/http/axios';
import { RrNParams, RrNotifListGetResultModel } from './model/rrnotification/rrnModel';

enum Api {
  RRN_LIST = '/rrn/getReturnReqNotifList',
  // RR_DETAIL = '/rr/viewbycdtdetail',
  RRN_INFO = '/rrn/viewbycdtinfo',
}

/**
 * @description: Get sample list value
 */

export const rrnListApi = (params: RrNParams) =>
  defHttp.get<RrNotifListGetResultModel>({
    url: Api.RRN_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

// export const rrDetailListApi = (params) =>
//   defHttp.post<RrnDetailInfoGetResultModel>({
//     url: Api.RRN_DETAIL,
//     timeout: 10000,
//     headers: {
//       // @ts-ignore
//       ignoreCancelToken: true,
//     },
//     data: { id: params },
//   });

export const rrnInfoListApi = (params) =>
  defHttp.post<RrNotifListGetResultModel>({
    url: Api.RRN_INFO,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });
