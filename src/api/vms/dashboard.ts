import { defHttp } from '/@/utils/http/axios';

import { poListGetResultModel } from './model/dashboard/poListModel';
import { raListGetResultModel } from './model/dashboard/raListModel';
import { rarListGetResultModel } from './model/dashboard/rarListModel';
import { pfiListGetResultModel } from './model/dashboard/pfiListModel';
import { pfirListGetResultModel } from './model/dashboard/pfirListModel';
import { invListGetResultModel } from './model/dashboard/invListModel';

enum Api {
  PO_LIST = '/dashponew/getdashponew',
  RA_LIST = '/dash/getdashranew',
  RA_AWAITING_LIST = '/dash/getdashra',
  RAR_AWAITING_LIST = '/dash/getdashrar',
  RAR2_AWAITING_LIST = '/dash/getdashrar2',
  PFI_LIST = '/dash/getdashpfi',
  PFI_AWAITING_LIST = '/dash/getdashpfiaa',
  INV_AWAITING_LIST = '/dash/getdashinv',
  PFIR_AWAITING_LIST = '/dash/getdashpfir',
}

/**
 * @description: Get sample list value
 */

export const poListApi = (params) =>
  defHttp.get<poListGetResultModel>({
    url: Api.PO_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const raListApi = (params) =>
  defHttp.get<raListGetResultModel>({
    url: Api.RA_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const raAwaitingListApi = (params) =>
  defHttp.get<raListGetResultModel>({
    url: Api.RA_AWAITING_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rarAwaitingListApi = (params) =>
  defHttp.get<rarListGetResultModel>({
    url: Api.RAR_AWAITING_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rar2AwaitingListApi = (params) =>
  defHttp.get<rarListGetResultModel>({
    url: Api.RAR2_AWAITING_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const pfiListApi = (params) =>
  defHttp.get<pfiListGetResultModel>({
    url: Api.PFI_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const pfiAwaitingListApi = (params) =>
  defHttp.get<pfiListGetResultModel>({
    url: Api.PFI_AWAITING_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const invAwaitingListApi = (params) =>
  defHttp.get<invListGetResultModel>({
    url: Api.INV_AWAITING_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const pfirAwaitingListApi = (params) =>
  defHttp.get<pfirListGetResultModel>({
    url: Api.PFIR_AWAITING_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
