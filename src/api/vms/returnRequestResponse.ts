import { defHttp } from '/@/utils/http/axios';
import { RrrParams, RrrListGetResultModel } from './model/rrr/rrrListModel';
import { RrrDetailInfoGetResultModel } from './model/rrr/rrrDetailInfoModel';
import { RrrItemListModel } from './model/rrr/rrrItemListModel';
import { RrrSaveParams, RrrSaveResultModel } from './model/rrr/rrrSaveModel';
import { RrrKirimParams, RrrKirimResultModel } from './model/rrr/rrrKirimModel';

enum Api {
  RRR_LIST = '/rrr/getReturnReqResponseList',
  RRR_DETAIL = '/rrr/viewbycdtdetail',
  RRR_INFO = '/rrr/viewbycdtinfo',
  RRR_SAVE = '/rrr/editRRR',
  RRR_SEND = '/rrr/kirimRRR',
}

/**
 * @description: Get sample list value
 */

export const rrrListApi = (params: RrrParams) =>
  defHttp.get<RrrListGetResultModel>({
    url: Api.RRR_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rrrDetailListApi = (params) =>
  defHttp.post<RrrDetailInfoGetResultModel>({
    url: Api.RRR_DETAIL,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rrrInfoListApi = (params) =>
  defHttp.post<RrrItemListModel>({
    url: Api.RRR_INFO,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

  export const rrrSaveApi = (params: RrrSaveParams) =>
  defHttp.post<RrrSaveResultModel>({
    url: Api.RRR_SAVE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const rrrKirimApi = (params: RrrKirimParams) =>
  defHttp.post<RrrKirimResultModel>({
    url: Api.RRR_SEND,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

