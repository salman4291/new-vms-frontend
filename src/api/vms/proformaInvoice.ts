import { defHttp } from '/@/utils/http/axios';
import { PfiParams, PfiListGetResultModel } from './model/pfi/pfiListModel';
import { PfiItemListModel } from './model/pfi/pfiItemListModel';
import { PfiDetailInfoGetResultModel } from './model/pfi/pfiDetailInfoModel';
import { PfiActionParams, PfiActionResultModel } from './model/pfi/pfiActionModel';
import { PfiRevertParams, PfiRevertResultModel } from './model/pfi/pfiRevertModel';

enum Api {
  PFI_LIST = '/pfi/getPfiAll',
  PFI_DETAIL = `/pfi/viewbycdtdetail`,
  PFI_INFO = '/pfi/viewbycdtinfo',
  PFI_ACTION = '/pfi/actionPfi',
  PFI_REVERT = '/pfi/actionBack',
}

/**
 * @description: Get sample list value
 */

export const pfiListApi = (params: PfiParams) =>
  defHttp.get<PfiListGetResultModel>({
    url: Api.PFI_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const pfiDetailApi = (params) =>
  defHttp.post<PfiItemListModel>({
    url: Api.PFI_DETAIL,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const pfiInfoApi = (params) =>
  defHttp.post<PfiDetailInfoGetResultModel>({
    url: Api.PFI_INFO,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const pfiActionApi = (params: PfiActionParams) =>
  defHttp.post<PfiActionResultModel>({
    url: Api.PFI_ACTION,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const pfiRevertApi = (params: PfiRevertParams) =>
  defHttp.post<PfiRevertResultModel>({
    url: Api.PFI_REVERT,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
