import { defHttp } from '/@/utils/http/axios';
import { RrParams, RrListGetResultModel } from './model/rr/rrListModel';
import { RrDetailInfoGetResultModel } from './model/rr/rrDetailInfoModel';
import { RrItemListModel } from './model/rr/rrItemListModel';
import { RRActionParams, RRActionResult } from './model/rr/RRActionModel';
import { RRSaveParams, RRSaveResultModel } from './model/rr/rrSaveModel';

enum Api {
  RR_LIST = '/rr/getReturnReqList',
  RR_DETAIL = '/rr/viewbycdtdetail',
  RR_INFO = '/rr/viewbycdtinfo',
  RR_SEND = '/rr/actionSent',
  RR_SAVE = '/rr/updateReturReq',
}

/**
 * @description: Get sample list value
 */

export const rrListApi = (params: RrParams) =>
  defHttp.get<RrListGetResultModel>({
    url: Api.RR_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rrDetailListApi = (params) =>
  defHttp.post<RrDetailInfoGetResultModel>({
    url: Api.RR_DETAIL,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rrInfoListApi = (params) =>
  defHttp.post<RrItemListModel>({
    url: Api.RR_INFO,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

  export const rrSendApi = (params:RRActionParams) =>
  defHttp.post<RRActionResult>({
    url: Api.RR_SEND,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const rrSaveApi = (params: RRSaveParams) =>
  defHttp.post<RRSaveResultModel>({
    url: Api.RR_SAVE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
