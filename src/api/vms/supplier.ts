import { SuppListGetResultModel, SuppParams } from './model/supp/suppListModel';
import {
  SuppListDataRegGetResultModel,
  SuppListDataRegParams,
} from './model/supp/suppListRegisteredModel';
import { SuppUsrListGetResultModel } from './model/supp/suppUsrModel';
import {
  SuppUsrDetailFilterParams,
  SuppUsrDetailListGetResultModel,
} from './model/supp/suppUsrDetailModel';
import { SuppRegisteredParams, SuppRegisteredGetResultModel } from './model/supp/suppRegModel';
import { suppCreateParams, suppCreateResultModel } from './model/supp/suppCreateModel';
import { suppEditParams, suppEditResultModel } from './model/supp/suppEditModel';
import { SuppRegDetail1ParamsGetResultModel } from './model/supp/suppRegDetail1Model';
import { SuppRegDetail2ParamsGetResultModel } from './model/supp/suppRegDetail2Model';
import { SuppRegDetail3ParamsGetResultModel } from './model/supp/suppRegDetail3Model';

import { defHttp } from '/@/utils/http/axios';

enum Api {
  SUPP = '/s/getSupplierAll',
  USR_SUPP = '/s/userSupplier',
  USR_DETAIL_SUPP = '/s/getUserSupplier',
  LIST_DATA_REG_SUPP = '/supplier/ldr',
  CREATE_SUPP = '/s/createSupplier',
  EDIT_SUPP = '/s/editUserSupplier',
  REG_SUPP = '/s/suppRegList',
  REG_DETAIL_1 = '/s/suppRegDetail1',
  REG_DETAIL_2 = '/s/suppRegDetail2',
  REG_DETAIL_3 = '/s/suppRegDetail3',
}

/**
 * @description: Get sample list value
 */

export const suppListApi = (params: SuppParams) =>
  defHttp.get<SuppListGetResultModel>({
    url: Api.SUPP,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const suppUsrApi = (params) =>
  defHttp.post<SuppUsrListGetResultModel>({
    url: Api.USR_SUPP,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const suppUsrDetailApi = (params: SuppUsrDetailFilterParams) =>
  defHttp.get<SuppUsrDetailListGetResultModel>({
    url: Api.USR_DETAIL_SUPP,
    params,
    data: params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const supplistDataRegisterdApi = (params: SuppListDataRegParams) =>
  defHttp.get<SuppListDataRegGetResultModel>({
    url: Api.LIST_DATA_REG_SUPP,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const suppRegApi = (params: SuppRegisteredParams) =>
  defHttp.get<SuppRegisteredGetResultModel>({
    url: Api.REG_SUPP,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const suppCreateApi = (params: suppCreateParams) =>
  defHttp.post<suppCreateResultModel>({
    url: Api.CREATE_SUPP,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const suppEditApi = (params: suppEditParams) =>
  defHttp.post<suppEditResultModel>({
    url: Api.EDIT_SUPP,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

// suppiler registered detail
export const suppRegDetail1 = (params) =>
  defHttp.post<SuppRegDetail1ParamsGetResultModel>({
    url: Api.REG_DETAIL_1,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const suppRegDetail2 = (params) =>
  defHttp.post<SuppRegDetail2ParamsGetResultModel>({
    url: Api.REG_DETAIL_2,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const suppRegDetail3 = (params) =>
  defHttp.post<SuppRegDetail3ParamsGetResultModel>({
    url: Api.REG_DETAIL_3,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
