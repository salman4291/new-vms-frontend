import { defHttp } from '/@/utils/http/axios';
import { TrfKItemListModel, TrfKItemListParams } from './model/trfk/trfkItemListModel';

enum Api {
  TRFK_LIST = '/tk/getTrfKwitansiist',
	TRFK_DETAIL = '/tk/getTKDetailPdf',
}

/**
 * @description: Get sample list value
 */

export const trfkListApi = (params: TrfKItemListParams) =>
  defHttp.get<TrfKItemListModel>({
    url: Api.TRFK_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const trfkDetailApi = (params) =>
  defHttp.get({
    url: Api.TRFK_DETAIL,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
