import { SLADetailInfoGetResultModel } from './model/sla/slaDetailInfoModel';
import { SLAItemListModel } from './model/sla/slaItemListModel';
import { SLAListGetResultModel, SLAParams } from './model/sla/slaListModel';
import { SlaCloseResultModel, SlaCloseParams } from './model/sla/slaCloseModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  SLA_LIST = '/apl/getConsLitigasiList',
  SLA_DETAIL = '/apl/viewbycdtdetail',
  SLA_INFO = '/apl/viewbycdtinfo',
  SLA_ACTION = '/apl/actionPfi',
  SLA_REVERT = '/apl/actionBack',
  SLA_CLOSE = '/apl/actionClosed',
}

/**
 * @description: Get sample list value
 */

export const slaListApi = (params: SLAParams) =>
  defHttp.get<SLAListGetResultModel>({
    url: Api.SLA_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const slaDetailApi = (params) =>
  defHttp.post<SLAItemListModel>({
    url: Api.SLA_DETAIL,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const slaInfoApi = (params) =>
  defHttp.post<SLADetailInfoGetResultModel>({
    url: Api.SLA_INFO,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const slaCloseApi = (params: SlaCloseParams) =>
  defHttp.post<SlaCloseResultModel>({
    url: Api.SLA_CLOSE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
