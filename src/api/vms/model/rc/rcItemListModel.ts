/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RcItemListParams = BasicPageParams;

export interface RcItemList {
  id: number;
  rtn_confirmation_number: string;
  rtn_confirmation_date: string;
  departemen: string;
  store: string;
  picked_up_date: number;
  name: number;
  supplier_code: number;
  phone: number;
  fax: string;
}

/**
 * @description: Request list return value
 */
export type RcItemListModel = BasicFetchResult<RcItemList>;
