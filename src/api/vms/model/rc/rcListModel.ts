import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RcParams = BasicPageParams;

// TODO
export interface RcListItem {
  id: string;
  rtn_confirmation_number: string;
  rtn_confirmation_date: number;
  store: string;
  departemen: string;
  status: string;
  supplier: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type RcListGetResultModel = BasicFetchResult<RcListItem>;
