/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RcDetailInfoParams = BasicPageParams;

export interface RcDetailInfo {
  id: string;
  no: string;
  nama_barang: string;
  barcode: string;
  qty_dikembalikan: number;
  harga_unit: number;
  nilai_pengembalian: number;
  keterangan: string;
  terima: string;
  total_harga: string;
  id_rr: string;
}

/**
 * @description: Request list return value
 */
export type RcDetailInfoGetResultModel = BasicFetchResult<RcDetailInfo>;
