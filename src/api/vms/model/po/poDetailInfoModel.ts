/**
 * @description: Request list interface parameters
 */
export interface PoDetailInfoParams {
  id: string;
}

export interface PoDetailInfo {
  id: string;
  sender_code: string;
  dept_code: string;
  department_name: string;
  delivery_to: string;
  order_date: string; //Date
  date_updated: string; //Date
  expected_delivery_date: string; //Date
  business_unit_name: string;
  business_unit_registration: string;
  business_unit_address: string;
  supplier_name: string;
  supplier_code: string;
  supplier_phone: string;
  supplier_fax_number: string;
  total_amount: number;
}

/**
 * @description: Request list return value
 */
export type PoDetailInfoGetResultModel = { items: PoDetailInfo[] };
