import { BasicPageParams } from '/@/api/model/baseModel';
import { BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type PoItemParams = BasicPageParams & { purchase_order: string };

// TODO
export interface PoItemListItem {
  line_no: string;
  item_code: string;
  sub_code: string;
  unit_code: string;
  capacity: string;
  barcode: string;
  item_name: string;
  sub_code_name: string;
  order_qty_in_pack: number;
  purchase_price_type: string;
  free_qty_insku: number;
  qty_per_pack: number;
}

export interface PoItemListItemResult {
  items: PoItemListItem[];
}

/**
 * @description: Request list return value
 */
export type PoItemListGetResultModel = BasicFetchResult<PoItemListItemResult>;
