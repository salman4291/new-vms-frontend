import { BasicFetchResult } from '/@/api/model/baseModel';

export interface Store2 {
  store_id: string,
  name: string,
  address: string,
}

export type Store2ListGetResultModel = BasicFetchResult<Store2>;