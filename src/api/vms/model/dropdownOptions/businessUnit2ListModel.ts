import { BasicFetchResult } from '/@/api/model/baseModel';

export interface BusinessUnit2 {
  id: string,
  business_unit_code: string,
  business_unit_name: string,
  business_unit_address: string,
}

export type BusinessUnit2ListGetResultModel = BasicFetchResult<BusinessUnit2>;