import { BasicFetchResult } from '/@/api/model/baseModel';

export interface Role {
  id: string;
  name: string;
}

export type RoleListGetResultModel = BasicFetchResult<Role>;
