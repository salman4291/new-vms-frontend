import { BasicFetchResult } from '/@/api/model/baseModel';

export interface BusinessUnit {
  business_unit_code: string,
  business_unit_name: string,
}

export type BusinessUnitListGetResultModel = BasicFetchResult<BusinessUnit>;