import { BasicFetchResult } from '/@/api/model/baseModel';

export interface Store {
  store_id: string,
  name: string,
}

export type StoreListGetResultModel = BasicFetchResult<Store>;