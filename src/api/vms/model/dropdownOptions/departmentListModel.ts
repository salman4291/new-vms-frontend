import { BasicFetchResult } from '/@/api/model/baseModel';

export interface Department {
  code_id: string,
  name: string,
}

export type DepartmentListGetResultModel = BasicFetchResult<Department>;