import { BasicFetchResult } from '/@/api/model/baseModel';

export interface Supplier {
  code: string;
  name: string;
}

export type SupplierListGetResultModel = BasicFetchResult<Supplier>;
