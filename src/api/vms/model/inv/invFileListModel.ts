import { BasicPageParams, BasicFetchResult, BasicFetchResult2 } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type InvFileParams = BasicPageParams;
export type InvFileFilterParams = BasicPageParams & { i_id?: string };

// TODO
export interface InvFileListItem {
  tax_invoice_pdf_file_name: string;
}

/**
 * @description: Request list return value
 */
export type InvFileListGetResultModel = BasicFetchResult<InvFileListItem>;
