/**
 * @description: Request list interface parameters
 */
export interface InvSaveItemDetailParams {
  vat_correction: string;
  ppn_bm: string;
  ID: string;
}

export interface InvSaveItemDetailResult {
  i_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type InvSaveItemDetailResultModel = InvSaveItemDetailResult;
