/**
 * @description: Request list interface parameters
 */
export interface InvActionParams {
  id: string;
  // action: string;
  // editor_name: string;
}

export interface InvActionResult {
  i_id: string
  id?: string;
}

/**
 * @description: Request list return value
 */
export type InvActionResultModel = InvActionResult;
