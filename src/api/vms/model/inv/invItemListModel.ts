/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type InvItemListParams = BasicPageParams;

export interface InvItemList {
  poi_id: number;
  id: number;
  kode_barang: string;
  sub_code: string;
  unit_code: string;
  kapasitas_barcode: string;
  nama_barang: string;
  sub_code_name_local: string;
  total_qty: number;
  harga_unit: number;
  harga_sebelum_pajak: number;
  harga_sesudah_pajak: number;
  pajak: string;
  ppn_bm: string;
  koreksi_harga_beli: number;
  koreksi_ppn: number;
  pajak_barang_mewah: number;
  nomor_order: number;
}

/**
 * @description: Request list return value
 */
export type InvItemListGetResultModel = BasicFetchResult<InvItemList>;
