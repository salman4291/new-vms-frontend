/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type InvDetailInfoParams = BasicPageParams;

export interface InvDetailInfo {
  nomor_order: string;
  tanggal_order: string;
  departemen: string;
  toko: string;
  nama_perusahaan: string;
  alamat_perusahaan: string;
  npwp: string;
  nama_supplier: string;
  alamat_supplier: string;
  kodepos: string;
  kota: string;
  negara: string;
  nama: string;
  telp_supplier: string;
  npwp_supp: string;
  nomor_seri_pajak: string;
  confirm_seri_pajak: string;
  tgl_faktur_pajak: string;
  no_inv_supplier: string;
  tgl_inv_supplier: string;
  revisi: string;
  status: string;
  sc_id: number;
  s_id: number;
  email: string;
  kode_supplier: string;
  fax_supplier: string;
  purchase_order: string;
}

/**
 * @description: Request list return value
 */
export type InvDetailInfoGetResultModel = BasicFetchResult<InvDetailInfo>;
