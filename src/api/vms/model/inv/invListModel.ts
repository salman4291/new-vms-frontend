import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type InvParams = BasicPageParams & { supplier_code?: string };
export type InvFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface InvListItem {
  purchase_order: string;
  supplier_code: string;
  total_net_amount: number;
  date_updated: string;
  status: string;
  revision: string;
  po: {
    supplier_name: string;
    store_code: string;
    delivery_to: string;
  };
}

/**
 * @description: Request list return value
 */
export type InvListGetResultModel = BasicFetchResult<InvListItem>;
