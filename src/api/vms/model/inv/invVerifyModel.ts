/**
 * @description: Request list interface parameters
 */
export interface InvVerifyParams {
  id: string;
  // editor_name: string;
}

export interface InvVerifyResult {
  i_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type InvVerifyResultModel = InvVerifyResult;
