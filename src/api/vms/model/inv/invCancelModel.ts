/**
 * @description: Request list interface parameters
 */
export interface InvCancelParams {
  id: string;
  // action: string;
  // editor_name: string;
}

export interface InvCancelResult {
  pfi_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type InvCancelModel = InvCancelResult;
