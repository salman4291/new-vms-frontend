import { BasicPageParams, BasicFetchResult, BasicFetchResult2 } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type InvFileTaxParams = BasicPageParams;
export type InvFileTaxFilterParams = BasicPageParams & { i_id?: string };

// TODO
export interface InvFileTaxListItem {
  seq: string;
  type: string;
  tax_invoice_pdf_file_name: string;
}

/**
 * @description: Request list return value
 */
export type InvFileTaxListGetResultModel = BasicFetchResult<InvFileTaxListItem>;
