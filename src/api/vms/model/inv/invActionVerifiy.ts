/**
 * @description: Request list interface parameters
 */
export interface invActionVerifyParams {
  id: string;
  // action: string;
  // editor_name: string;
}

export interface invActionVerifyResult {
  id?: string;
}

/**
 * @description: Request list return value
 */
export type invActionVerifyResultModel = invActionVerifyResult;
