/**
 * @description: Request list interface parameters
 */
export interface SlaCloseParams {
  id: string;
  status: string;
  // editor_name: string;
}

export interface SlaCloseResult {
  sla_id: string;
}

/**
 * @description: Request list return value
 */
export type SlaCloseResultModel = SlaCloseResult;
