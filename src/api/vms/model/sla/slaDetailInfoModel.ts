/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type SLADetailInfoParams = BasicPageParams;

export interface SLADetailInfo {
  cdt: string;
  no: string;
  sales_date: string;
  barcode: string;
  bank_promo: string;
  sales_type: string;
  nama_barang: string;
  sales_qty: string;
  selling_price: string;
  base_cons_margin: number;
  base_cons_payable: number;
  support_promo_sup: number;
  total_cons_payable: number;
  terima: string;
}

/**
 * @description: Request list return value
 */
export type SLADetailInfoGetResultModel = BasicFetchResult<SLADetailInfo>;
