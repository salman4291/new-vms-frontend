import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SLAParams = BasicPageParams;
export type SLAFilterParams = BasicPageParams & { lit_no?: string };

// TODO
export interface SLAListItem {
  show: boolean;
  id: string;
  supplier: string;
  store: string;
  department: string;
  sales_date: string;
  status: string;
  submit_date: string;
}

/**
 * @description: Request list return value
 */
export type SLAListGetResultModel = BasicFetchResult<SLAListItem>;
