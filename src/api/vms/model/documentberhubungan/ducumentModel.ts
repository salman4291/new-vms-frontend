/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type DocumentParams = BasicPageParams;

export interface DocParams {
  id: string;
}
export interface DocParams2 {
  id: string;
  pfi_id: string;
}
export interface DocParams3 {
  ra_id: string;
}
export interface DocParams4 {
  rar_id: string;
}
export interface DocParams5 {
  pfi_id: string;
}
export interface DocParams6 {
  pfir_id: string;
}
export interface DocParams7 {
  i_id: string;
}

export interface DocumentItemList {
  id: string;
  po_status: string;
  po_date: Date;
  po_revisi: string;
  ra_id: string;
  ra_status: string;
  ra_date: Date;
  ra_revisi: string;
  rar_id: string;
  rar_status: string;
  rar_date: Date;
  rar_revisi: string;
  pfi_id: string;
  pfi_status: string;
  pfi_date: Date;
  pfi_revisi: string;
  pfir_id: string;
  pfir_status: string;
  pfir_date: Date;
  pfir_revisi: string;
  i_id: string;
  i_status: string;
  i_date: Date;
  i_revisi: string;
  
}

/**
 * @description: Request list return value
 */
export type DocumentItemListModel = BasicFetchResult<DocumentItemList>;
