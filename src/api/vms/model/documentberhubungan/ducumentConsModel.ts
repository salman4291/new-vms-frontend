/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type DocumentConsParams = BasicPageParams;

export interface DocConsParams {
  cdt: string;
}

export interface DocumentConsItemList {
  cdt: string;
  cpfi_status: string;
  cpfi_date: Date;
  cpfi_revisi: string;
  ci_status: string;
  ci_date: Date;
  ci_revisi: string;
}

/**
 * @description: Request list return value
 */
export type DocumentCosnItemListModel = BasicFetchResult<DocumentConsItemList>;
