/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type DocumentReturParams = BasicPageParams;

export interface DocReturParams {
  id: string;
}
export interface DocReturParams2 {
  id: string;
  rc_id: string;
}

export interface DocumentReturList {
  id: string;
  rr_status: string;
  rr_date: Date;
  rr_number: string;
  rc_id: string;
  rc_status: string;
  rc_date: Date;
  rc_number: string;
}

/**
 * @description: Request list return value
 */
export type DocumentReturListModel = BasicFetchResult<DocumentReturList>;
