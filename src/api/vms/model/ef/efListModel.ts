import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type efParams = BasicPageParams;
//export type EfFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface EfListItem {
  show: boolean;
  id: string;
  namafile: string;
  nomor_seri_pajak: string;
  upload_date: string;
  download_date: string;
  status: string;
  supplier_code: string;
  supplier: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type EfListGetResultModel = BasicFetchResult<EfListItem>;
