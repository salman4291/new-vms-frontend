import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type PfirParams = BasicPageParams;
export type PfirFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface PfirListItem {
  id: string;
  referensi: string;
  merchant: string;
  store: string;
  revisi: string;
  tanggal_diterima: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type PfirListGetResultModel = BasicFetchResult<PfirListItem>;
