/**
 * @description: Request list interface parameters
 */
export interface PfirSaveParams {
  IS_ACCEPTED: string;
  RECONCILED: number;
  REMARKS: string;
  RAIID: string;
}

export interface PfirSaveResult {
  pfir_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type PfirSaveResultModel = PfirSaveResult;
