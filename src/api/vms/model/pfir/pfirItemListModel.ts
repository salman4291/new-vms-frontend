/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type pfirItemListParams = BasicPageParams;

export interface PfirItemList {
  line_no: string;
  item_code: string;
  sub_code: string;
  unit_code: string;
  capacity: string;
  barcode: string;
  item_name: string;
  unit_price: number;
  raipoi: [
    {
      received_qty: number;
      raipfii: [
        {
          reconciled: number;
          remarks: string;
        },
      ];
    },
  ];
}

/**
 * @description: Request list return value
 */
export type PfirItemListModel = BasicFetchResult<PfirItemList>;
