/**
 * @description: Request list interface parameters
 */
export interface PfirActionParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface PfirActionResult {
  pfir_id: [
    {
      id?: string;
    },
  ];
  id?: string;
}

/**
 * @description: Request list return value
 */
export type PfirActionResultModel = PfirActionResult;
