/**
 * @description: Request list interface parameters
 */
export interface PfirAcceptRejectParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface PfirAcceptRejectResult {
  invoice_id?: string;
  pfir_id?: string;
}

/**
 * @description: Request list return value
 */
export type PfirAcceptRejectResultModel = PfirAcceptRejectResult;
