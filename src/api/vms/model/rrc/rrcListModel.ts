import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RrcParams = BasicPageParams;

// TODO
export interface RrcListItem {
  id: string;
  merchant: string;
  toko: number;
  supplier_code: string;
  rc_date: string;
  download_date: string;
  supplier_name: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type RrcListGetResultModel = BasicFetchResult<RrcListItem>;
