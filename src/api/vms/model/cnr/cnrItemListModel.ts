/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type NrItemListParams = BasicPageParams;

export interface NrItemList {
  nomor_order: number;
  purchase_order: string;
  kode_barang: string;
  kapasitas_barcode: string;
  nama_barang: string;
  total_qty: number;
  harga_unit: number;
  harga_sebelum_pajak: number;
  harga_setelah_pajak: number;
  persen_pajak: string;
  ppn_bm: string;
  sub_total: number;
}

/**
 * @description: Request list return value
 */
export type NrItemListModel = BasicFetchResult<NrItemList>;
