import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type CNrParams = BasicPageParams;

// TODO
export interface CNrListItem {
  id: string;
  nomor_retur: string;
  nomor_pfi: number;
  supplier_code: string;
  pfi_date: string;
  download_date: string;
  supplier_name: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type CNrListGetResultModel = BasicFetchResult<CNrListItem>;
