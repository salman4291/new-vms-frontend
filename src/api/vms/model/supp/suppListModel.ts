import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppParams = BasicPageParams;
export type SuppFilterParams = BasicPageParams & { code?: string };

// TODO
export interface SuppListItem {
  id: string;
  code: string;
  local_name: string;
  tax_id: string;
  stop_business_date: string;
}

/**
 * @description: Request list return value
 */
export type SuppListGetResultModel = BasicFetchResult<SuppListItem>;
