import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppRegDetail3Params = BasicPageParams;

// TODO
export interface SuppRegDetail3Item {
  id_accountuser: number;
  id_supp_contact: number;
  s_id: number;
  first_name: string;
  last_name: string;
  username: string;
  account_enable: string;
}

/**
 * @description: Request list return value
 */
export type SuppRegDetail3ParamsGetResultModel = BasicFetchResult<SuppRegDetail3Item>;
