/**
 * @description: Request list interface parameters
 */
export interface suppCreateParams {
  contact_person: string;
  username: string;
  password: string;
  email: string;
  additional_email: string;
}

export interface suppCreateResult {
  id?: number;
}

/**
 * @description: Request list return value
 */
export type suppCreateResultModel = suppCreateResult;
