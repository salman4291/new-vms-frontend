import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppRegDetail2Params = BasicPageParams;

// TODO
export interface SuppRegDetail2Item {
  s_id: number;
  vs_id: number;
  department: string;
  contact_person_md: string;
  phone_md: string;
  address_md: string;
  suppliergln_md: string;
  fax_md: string;
}

/**
 * @description: Request list return value
 */
export type SuppRegDetail2ParamsGetResultModel = BasicFetchResult<SuppRegDetail2Item>;
