import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppListDataRegParams = BasicPageParams;

// TODO
export interface SuppListRegisteredItem {
  id: string;
  code: string;
  nama: string;
  search: string;
}

/**
 * @description: Request list return value
 */
export type SuppListDataRegGetResultModel = BasicFetchResult<SuppListRegisteredItem>;
