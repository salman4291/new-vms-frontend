import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppUsrDetailParams = BasicPageParams;
export type SuppUsrDetailFilterParams = { id?: number };

// TODO
export interface SuppUsrDetailListItem {
  id: number;
  mversion: string;
  enabled: string;
  username: string;
  email: string;
  additional_email: string;
  mobile_number: string;
  content_type: string;
}

/**
 * @description: Request list return value
 */
export type SuppUsrDetailListGetResultModel = BasicFetchResult<SuppUsrDetailListItem>;
