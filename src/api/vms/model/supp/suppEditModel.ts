import { BasicFetchResult } from '/@/api/model/baseModel';

/**
 * @description: Request list interface parameters
 */
export interface suppEditParams {
  contact_person: string;
  email: string;
  additional_email: string;
  au_id?: number;
}

/**
 * @description: Request list return value
 */
export type suppEditResultModel = BasicFetchResult<suppEditParams>;
