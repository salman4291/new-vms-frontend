import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppRegDetail1Params = BasicPageParams;

// TODO
export interface SuppRegDetail1Item {
  s_id: number;
  sd_id: number;
  email_primary: string;
  phone_primary: string;
  register_date: string;
  b2b: string;
  supplier_code: string;
  english_name: string;
  tax_id: string;
}

/**
 * @description: Request list return value
 */
export type SuppRegDetail1ParamsGetResultModel = BasicFetchResult<SuppRegDetail1Item>;
