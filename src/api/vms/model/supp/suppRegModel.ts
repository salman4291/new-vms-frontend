import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppRegisteredParams = BasicPageParams;

// TODO
export interface SuppRegisteredItem {
  s_id: number;
  kode_supplier: string;
  nama_local: string;
  id_pajak: string;
  status: string;
  p2p: string;
  search: string;
}

/**
 * @description: Request list return value
 */
export type SuppRegisteredGetResultModel = BasicFetchResult<SuppRegisteredItem>;
