import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type SuppUsrParams = BasicPageParams;

// TODO
export interface SuppUsrListItem {
  id: string;
  username: string;
  email: string;
  enabled: string;
  last_login: string;
}

/**
 * @description: Request list return value
 */
export type SuppUsrListGetResultModel = BasicFetchResult<SuppUsrListItem>;
