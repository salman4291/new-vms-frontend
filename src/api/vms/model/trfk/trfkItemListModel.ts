/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type TrfKItemListParams = BasicPageParams;

export interface TrfKItemList {
  id: number;
  nomor_kwitansi: string;
  created_date: string;
  download_date: string;
  supplier_code: string;
  supplier_name: number;
  business_unit: number;
  supplier: number;
}

/**
 * @description: Request list return value
 */
export type TrfKItemListModel = BasicFetchResult<TrfKItemList>;
