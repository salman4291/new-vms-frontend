/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RrrDetailInfoParams = BasicPageParams;

export interface RrrDetailInfo {
  id: string;
  rrir_id: string;
  no: string;
  item_code: string;
  nama_barang: string;
  barcode: string;
  qty_dikembalikan: number;
  harga_unit: number;
  nilai_pengembalian: number;
  keterangan: string;
  terima: string;
  keterangan_1: string;
}

/**
 * @description: Request list return value
 */
export type RrrDetailInfoGetResultModel = BasicFetchResult<RrrDetailInfo>;
