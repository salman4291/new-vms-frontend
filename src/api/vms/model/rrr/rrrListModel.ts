import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RrrParams = BasicPageParams;

// TODO
export interface RrrListItem {
  id: string;
  return_request_number: string;
  return_request_date: number;
  store: string;
  departemen: string;
  supplier: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type RrrListGetResultModel = BasicFetchResult<RrrListItem>;
