/**
 * @description: Request list interface parameters
 */
export interface RrrSaveParams {
  rrir_id: string;
  terima: string;
}

export interface RrrSaveResult {
  id?: string;
  rrir_id?: string;
}

/**
 * @description: Request list return value
 */
export type RrrSaveResultModel = RrrSaveResult;
