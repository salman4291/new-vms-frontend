/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RrrItemListParams = BasicPageParams;

export interface RrrItemList {
  id: string;
  status: string;
  return_request_number: string;
  rtn_request_date: string;
  kapasitas_barcode: string;
  departemen: string;
  store: number;
  pick_up_location: number;
  supplier_name: number;
  supplier_code: number;
  supplier_phone: string;
  supplier_fax: string;
}

/**
 * @description: Request list return value
 */
export type RrrItemListModel = BasicFetchResult<RrrItemList>;
