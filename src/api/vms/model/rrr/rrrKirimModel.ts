/**
 * @description: Request list interface parameters
 */
export interface RrrKirimParams {
  id: string;
  // editor_name: string;
}

export interface RrrKirimResult {
  id?: string;
}

/**
 * @description: Request list return value
 */
export type RrrKirimResultModel = RrrKirimResult;
