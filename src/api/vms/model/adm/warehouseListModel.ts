import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type WParams = BasicPageParams;

// TODO
export interface WListItem {
  id: string;
  warehouse_code: string;
  warehouse_name: string;
  toko: string;
  int_code: string;
  for_interface: string;
  search: string;
}

/**
 * @description: Request list return value
 */
export type WListGetResultModel = BasicFetchResult<WListItem>;
