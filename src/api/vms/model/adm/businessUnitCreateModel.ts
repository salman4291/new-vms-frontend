/**
 * @description: Request list interface parameters
 */

import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type businessUnitDetailInfoParams = BasicPageParams;

export interface BusinessUnitSaveParams {
  id: string;
  setid: string;
  business_unit_code: string;
  business_unit_name: string;
  business_unitgln: string;
  business_unit_address: string;
  business_unit_registration: string;
  hm_od_directory: string;
  ima_store_prefix: string;
}

export interface BusinessUnitSaveResult {
  id: string;
  setid: string;
  business_unit_code: string;
  business_unit_name: string;
  business_unitgln: string;
  business_unit_address: string;
  business_unit_registration: string;
  hm_od_directory: string;
  ima_store_prefix: string;
  
  // id?: string;
}

/**
 * @description: Request list return value
 */
// export type StoreSaveResultModel = StoreSaveResult;

export type BusinessUnitSaveResultModel = BasicFetchResult<BusinessUnitSaveParams>;

