import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type BUParams = BasicPageParams;

// TODO
export interface BUListItem {
  id: string;
  kode: string;
  nama: string;
  alamat: string;
  registrasi: string;
  setid: string;
  search: string;
}

/**
 * @description: Request list return value
 */
export type BUListGetResultModel = BasicFetchResult<BUListItem>;
