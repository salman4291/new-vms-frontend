import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type IUParams = BasicPageParams;

// TODO
export interface IUListItem {
  id: string;
  first_name: string;
  username: string;
  last_name: string;
  role: string;
  enabled: string;
  email: string;
  role_id: string;
  search: string;
}

/**
 * @description: Request list return value
 */
export type IUListGetResultModel = BasicFetchResult<IUListItem>;
