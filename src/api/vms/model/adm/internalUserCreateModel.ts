/**
 * @description: Request list interface parameters
 */

import { List } from 'echarts';
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type internalUserDetailInfoParams = BasicPageParams;

export interface InternalUserSaveParams {
  id: string;
  first_name: string;
  username: string;
  last_name: string;
  email: string;
  role: List;
}

export interface InternalUserSaveResult {
  //items: string;
  id: string;
  first_name: string;
  username: string;
  last_name: string;
  email: string;
  role: List;
  
  // id?: string;
}

/**
 * @description: Request list return value
 */
// export type StoreSaveResultModel = StoreSaveResult;

export type InternalUserSaveResultModel = BasicFetchResult<InternalUserSaveParams>;

