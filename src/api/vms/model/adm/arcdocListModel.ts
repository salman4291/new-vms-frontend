import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type ArcDocParams = BasicPageParams;

// TODO
export interface ArcDocListItem {
  id: string;
  create_on: string;
  supplier_code: string;
  store_code: string;
  status: string;
  search: string;
}

/**
 * @description: Request list return value
 */
export type ArcDocListGetResultModel = BasicFetchResult<ArcDocListItem>;
