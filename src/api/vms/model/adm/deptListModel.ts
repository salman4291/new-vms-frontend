import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type DeptParams = BasicPageParams;

// TODO
export interface DeptListItem {
  code_id: string;
  name: string;
  local_name: string;
  last_update: string;
}

/**
 * @description: Request list return value
 */
export type DeptListGetResultModel = BasicFetchResult<DeptListItem>;
