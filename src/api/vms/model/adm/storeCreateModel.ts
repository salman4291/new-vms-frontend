/**
 * @description: Request list interface parameters
 */

import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type storeDetailInfoParams = BasicPageParams;

export interface StoreSaveParams {
  store_id: string;
  name: string;
  storegln: string;
  c4business_unit: string;
  logistic_flag: string;
  // address: string;
  created_by: string;
  last_updated_by: string;
}

export interface StoreSaveResult {
  //items: string;
  store_id: string;
  name: string;
  gln: string;
  business_unit: string;
  is_logistic: string;
  
  // id?: string;
}

/**
 * @description: Request list return value
 */
// export type StoreSaveResultModel = StoreSaveResult;

export type StoreSaveResultModel = BasicFetchResult<StoreSaveParams>;

