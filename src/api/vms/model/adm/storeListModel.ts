import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type StoreParams = BasicPageParams;

// TODO
export interface StoreListItem {
  store_id: string;
  name: string;
  storegln: string;
}

/**
 * @description: Request list return value
 */
export type StoreListGetResultModel = BasicFetchResult<StoreListItem>;
