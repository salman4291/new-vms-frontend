/**
 * @description: Request list interface parameters
 */

import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type warehouseDetailInfoParams = BasicPageParams;

export interface WarehouseSaveParams {
  id: string;
  warehouse_code: string;
  warehouse_name: string;
  int_code: string;
  store_code: string;
  for_interface: string;
  address: string;
}

export interface WarehouseSaveResult {
  id: string;
  warehouse_code: string;
  warehouse_name: string;
  int_code: string;
  store_code: string;
  for_interface: string;
  
  // id?: string;
}

/**
 * @description: Request list return value
 */
// export type StoreSaveResultModel = StoreSaveResult;

export type WarehouseSaveResultModel = BasicFetchResult<WarehouseSaveParams>;

