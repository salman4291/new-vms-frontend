import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type pfiParams = BasicPageParams;

// TODO
export interface pfiListItem {
  id: string;
  url: string;
  pro_forma_invoice_date: string;
  delivery_to: string;
  status: string;
  supplier_code: string;
  purchase_order: string;
}

/**
 * @description: Request list return value
 */
export type pfiListGetResultModel = BasicFetchResult<pfiListItem>;
