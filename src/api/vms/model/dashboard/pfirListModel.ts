import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type pfirParams = BasicPageParams;

// TODO
export interface pfirListItem {
  id: string;
  url: string;
  pro_forma_invoice_date: string;
  delivery_to: string;
  status: string;
  supplier_code: string;
  purchase_order: string;
}

/**
 * @description: Request list return value
 */
export type pfirListGetResultModel = BasicFetchResult<pfirListItem>;
