import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type raParams = BasicPageParams;

// TODO
export interface raListItem {
  id: string;
  url: string;
  receiving_advice_date: string;
  delivery_to: string;
  status: string;
  supplier_code: string;
  purchase_order: string;
}

/**
 * @description: Request list return value
 */
export type raListGetResultModel = BasicFetchResult<raListItem>;
