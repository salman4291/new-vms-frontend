import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type invParams = BasicPageParams;

// TODO
export interface invListItem {
  id: string;
  url: string;
  invoice_date: string;
  delivery_to: string;
  status: string;
  supplier_code: string;
  purchase_order: string;
}

/**
 * @description: Request list return value
 */
export type invListGetResultModel = BasicFetchResult<invListItem>;
