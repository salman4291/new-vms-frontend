import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type poParams = BasicPageParams;

// TODO
export interface poListItem {
  id: string;
  url: string;
  order_date: string;
  delivery_to: string;
  status: string;
  supplier_code: string;
}

/**
 * @description: Request list return value
 */
export type poListGetResultModel = BasicFetchResult<poListItem>;
