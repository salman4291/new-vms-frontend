/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RrItemListParams = BasicPageParams;

export interface RrItemList {
  id: number;
  rtn_req_number: string;
  rtn_req_date: string;
  department: string;
  toko: string;
  pick_up_location: number;
  kode_supplier: number;
  nama_supplier: number;
  telepon: number;
  fax: string;
  status: string;
}

/**
 * @description: Request list return value
 */
export type RrItemListModel = BasicFetchResult<RrItemList>;
