/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RrDetailInfoParams = BasicPageParams;

export interface RrDetailInfo {
  id: string;
  id_rr: string;
  no: string;
  item_code: string;
  nama_barang: string;
  barcode: string;
  qty_dikembalikan: number;
  harga_unit: number;
  nilai_pengembalian: number;
  keterangan: string;
  terima: string;
  keterangan1: string;
  total_harga: number;
}

/**
 * @description: Request list return value
 */
export type RrDetailInfoGetResultModel = BasicFetchResult<RrDetailInfo>;
