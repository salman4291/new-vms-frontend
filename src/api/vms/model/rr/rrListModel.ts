aimport { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RrParams = BasicPageParams;

// TODO
export interface RrListItem {
  id: string;
  return_request_number: string;
  rtn_request_date: number;
  store: string;
  departemen: string;
  status: string;
  supplier: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type RrListGetResultModel = BasicFetchResult<RrListItem>;
