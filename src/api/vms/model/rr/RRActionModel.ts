/**
 * @description: Request list interface parameters
 */

import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RRDetailInfoParams = BasicPageParams;
export interface RRActionParams {
  id: string;
  // action: string;
  // editor_name: string;
}

export interface RRActionResult {
  items: {
    return_request_number:string;
    id?: string;
  }
}

/**
 * @description: Request list return value
 */
export type RRActionResultModel =  RRActionResult;
