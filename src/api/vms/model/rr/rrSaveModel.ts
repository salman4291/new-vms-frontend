/**
 * @description: Request list interface parameters
 */
export interface RRSaveParams {
  ACCEPTED: string;
  REMARKS: string;
  id: string;
}

export interface RRSaveResult {
  rr_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type RRSaveResultModel = RRSaveResult;
