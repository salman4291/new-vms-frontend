/**
 * @description: Request list interface parameters
 */
export interface RarRevertParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface RarRevertResult {
  ra_id: string;
}

/**
 * @description: Request list return value
 */
export type RarRevertResultModel = RarRevertResult;
