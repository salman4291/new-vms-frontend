/**
 * @description: Request list interface parameters
 */
export interface RarSaveParams {
  IS_ACCEPTED: string;
  RECEIVED_QTY: number;
  REMARKS: string;
  ID: string;
}

export interface RarSaveResult {
  rar_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type RarSaveResultModel = RarSaveResult;
