/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RarItemListParams = BasicPageParams;

export interface RarItemList {
  id: string;
  line_no: number;
  item_code: string;
  sub_code: string;
  unit_code: string;
  capacity: string;
  barcode: string;
  item_name: string;
  sub_code_name: string;
  free_qty_insku: number;
  order_qty_insku: number;
  raipoi: [
    {
      id: number;
      is_revised: string;
      received_qty: number;
      remarks: Text;
    },
  ];
}

// interface FetchResult<T> {
//   POI: T[];
//   items: { purchase_order: string };
//   total: number;
// }

/**
 * @description: Request list return value
 */
export type RarItemListModel = BasicFetchResult<RarItemList>;
