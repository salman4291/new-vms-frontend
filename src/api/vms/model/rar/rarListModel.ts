import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RarParams = BasicPageParams;
export type RarFilterParams = BasicPageParams & { purchase_order?: string };

export interface RarListItem {
  purchase_order: string;
  receiving_advice_number: string;
  //revision_number: string;
  receiving_advice_date: string;
  status: string;
  supplier_name: string;
  store: string;
  business_unit_name: string;
}

/**
 * @description: Request list return value
 */
export type RarListGetResultModel = BasicFetchResult<RarListItem>;
