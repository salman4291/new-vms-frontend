/**
 * @description: Request list interface parameters
 */
export interface RarActionParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface RarActionResult {
  rar_id?: string;
  pfi_id?: string;
}

/**
 * @description: Request list return value
 */
export type RarActionResultModel = RarActionResult;
