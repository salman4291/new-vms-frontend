/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type UbpfrParams = BasicPageParams;

export interface UbpfrItemList {
  id: number;
  namafile: string;
  tanggal_upload: string;
  jumlah_insert: string;
  jumlah_revisi: string;
  jumlah_gagal: string;
  gagal_upload: string;
}

/**
 * @description: Request list return value
 */
export type UbpfrItemListModel = BasicFetchResult<UbpfrItemList>;
