/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type LbpfrParams = BasicPageParams;

export interface LbpfrItemList {
  id: number;
  nomor_invoice: string;
  supplier_code: string;
  supplier_name: string;
  invoice_date: string;
  download_date: string;
  status: string;
  business_unit: string;
  supplier: string;
}

/**
 * @description: Request list return value
 */
export type LbpfrItemListModel = BasicFetchResult<LbpfrItemList>;
