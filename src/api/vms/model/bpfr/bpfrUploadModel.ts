import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type UploadMsgParams = BasicPageParams;

// TODO
export interface UploadMsg {
  message: string;
}

/**
 * @description: Request list return value
 */
export type UploadMsgGetResultModel = BasicFetchResult<UploadMsg>;
