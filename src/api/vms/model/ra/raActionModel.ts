/**
 * @description: Request list interface parameters
 */
export interface RaActionParams {
  id: string;
  action: string;
  editor_name?: string;
}

export interface RaActionResult {
  pfi_id?: string;
  rar_id?: string;
}

/**
 * @description: Request list return value
 */
export type RaActionGetResultModel = RaActionResult;
