/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type raItemListParams = BasicPageParams & { purchase_order?: string };

export interface RaItemList {
  line_no: string;
  item_code: string;
  sub_code: string;
  unit_code: string;
  capacity: string;
  barcode: string;
  item_name: string;
  sub_code_name: string;
  order_qty_in_pack: number;
  purchase_price_type: string;
  free_qty_insku: number;
  qty_per_pack: number;
}

/**
 * @description: Request list return value
 */
export type RaItemListModel = BasicFetchResult<RaItemList>;
