import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RaParams = BasicPageParams;
export type RaFilterParams = BasicPageParams & { purchase_order?: string };

export interface RaListItem {
  purchase_order: string;
  supplier_name: string;
  revision_number: string;
  receiving_advice_number: string;
  receiving_advice_date: string;
  status: string;
  merchant: string;
  store: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type RaListGetResultModel = BasicFetchResult<RaListItem>;
