/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type raDetailInfoParams = BasicPageParams & { receiving_advice_number?: string };

export interface RaDetailInfo {
  receiving_advice_number: string;
  receiving_advice_date: string;
  status: string;
  po: {
    id: string;
    po_no: string;
    order_date: string;
    dept_code: string;
    store_code: string;
    delivery_to: string;
    supplier_name: string;
    supplier_code: string;
    supplier_phone: string;
    supplier_fax_number: string;
    recipient_code: string;
  };
}

/**
 * @description: Request list return value
 */
export type RaDetailInfoGetResultModel = BasicFetchResult<RaDetailInfo>;
