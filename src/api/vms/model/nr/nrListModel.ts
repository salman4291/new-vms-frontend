import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type NrParams = BasicPageParams;

// TODO
export interface NrListItem {
  id: string;
  retur_number: string;
  rc_number: number;
  supplier_code: string;
  rc_date: string;
  download_date: string;
  supplier_name: string;
  status: string;
  business_unit_code: string;
}

/**
 * @description: Request list return value
 */
export type NrListGetResultModel = BasicFetchResult<NrListItem>;
