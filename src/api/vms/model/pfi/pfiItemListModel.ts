/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type raItemListParams = BasicPageParams;

export interface PfiItemList {
  line_no: string;
  item_code: string;
  sub_code: string;
  unit_code: string;
  capacity: string;
  barcode: string;
  item_name: string;
  unit_price: number;
  received_qty: number;
  reconciled: number;
  remarks: string;
  pfi: number;
}

/**
 * @description: Request list return value
 */
export type PfiItemListModel = BasicFetchResult<PfiItemList>;
