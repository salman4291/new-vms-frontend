/**
 * @description: Request list interface parameters
 */
export interface PfiRevertParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface PfiRevertResult {
  ra_id: string;
}

/**
 * @description: Request list return value
 */
export type PfiRevertResultModel = PfiRevertResult;
