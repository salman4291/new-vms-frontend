/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type pfiDetailInfoParams = BasicPageParams;

export interface PfiDetailInfo {
  PFI: {
    id: string;
    receiving_advice: string;
    date_updated: string;
    revision: string;
  };
  RA: [
    {
      purchase_order: string;
      po: {
        sender_code: string;
        store_code: string;
        delivery_to: string;
        dept_code: string;
        department_name: string;
        po_no: string;
        order_date: string;
        supplier_name: string;
        supplier_code: string;
        supplier_phone: string;
        supplier_fax_number: string;
      };
    },
  ];
}

/**
 * @description: Request list return value
 */
export type PfiDetailInfoGetResultModel = BasicFetchResult<PfiDetailInfo>;
