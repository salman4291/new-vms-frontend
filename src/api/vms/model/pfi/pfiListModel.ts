import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type PfiParams = BasicPageParams;
export type PfiFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface PfiListItem {
  show: boolean;
  id: string;
  purchase_order: string;
  supplier_name: string;
  store: string;
  // delivery_to: string;
  revision: string;
  date_updated: string;
  status: string;
}

/**
 * @description: Request list return value
 */
export type PfiListGetResultModel = BasicFetchResult<PfiListItem>;
