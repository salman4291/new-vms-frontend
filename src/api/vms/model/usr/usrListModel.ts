import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type UsrParams = BasicPageParams;

// TODO
export interface UsrListItem {
  first_name: string;
  username: string;
  last_name: string;
  role: string;
  email: string;
  departemen: string;
  store: string;
}

/**
 * @description: Request list return value
 */
export type UsrListGetResultModel = BasicFetchResult<UsrListItem>;
