import { BasicPageParams } from '/@/api/model/baseModel';
import { BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type PrdItemParams = BasicPageParams & { purchase_order: string };

// TODO
export interface PrdItemListItem {
  id: number;
  business_unit: string;
  invoice: number;
  nomor_order: string;
  store: string;
  jatuh_tempo: string;
  harga: number;
  total_harga: number;
}

export interface PrdItemListItemResult {
  items: PrdItemParams[];
}

/**
 * @description: Request list return value
 */
export type PrdItemListGetResultModel = BasicFetchResult<PrdItemListItemResult>;
