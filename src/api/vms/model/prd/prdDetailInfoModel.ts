/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type prdDetailInfoParams = BasicPageParams;

export interface PrdInfo {
  id: string;
  atas_nama_pengirim: string;
  nomor_rekening_pengirim: string;
  nama_bank_pengirim: string;
  alamat_pengirim: string;
  cabang_pengirim: string;
  payment_reference: string;
  tanggal_pembayaran: string;
  atas_nama_penerima: string;
  nomor_rekening_penerima: string;
  nama_bank_penerima: string;
  alamat_penerima: string;
  cabang_penerima: string;
}

/**
 * @description: Request list return value
 */
export type PrdDetailInfoGetResultModel = BasicFetchResult<PrdInfo>;
