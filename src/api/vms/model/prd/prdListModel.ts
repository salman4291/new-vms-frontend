import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type PrdParams = BasicPageParams;
export type PrdFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface PrdListItem {
  show: boolean;
  id: string;
  tanggal_pembayaran: string;
  payment_refrensi: string;
  harga: string;
  merchant: string;
  status: string;
}

/**
 * @description: Request list return value
 */
export type PrdListGetResultModel = BasicFetchResult<PrdListItem>;
