/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type CInvDetailInfoParams = BasicPageParams;

export interface CInvDetailInfo {
  i_id: string;
  cdt: string;
  pfi_number: string;
  tanggal_pfi: string;
  toko: string;
  department: string;
  nama_perusahaan: string;
  alamat_perusahaan: string;
  npwp: string;
  sales_start_date: string;
  sales_end_date: string;
  cons_margin_rate: string;
  cons_margin_value: string;
  code: string;
  nama_perusahaan_supp: string;
  alamat_perusahaan_supp: string;
  kode_pos: string;
  kota: string;
  negara: string;
  nama: string;
  telepon: string;
  fax: string;
  email: number;
  nomor_seri_pajak: number;
  confirm_seri_pajak: string;
  tgl_faktur_pajak: string;
  no_inv_supplier: string;
  tgl_inv_supplier: string;
  revisi: string;
  status: string;
  sc_id: string;
  supp_id: string;
}

/**
 * @description: Request list return value
 */
export type CInvDetailInfoGetResultModel = BasicFetchResult<CInvDetailInfo>;
