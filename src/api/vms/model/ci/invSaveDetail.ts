/**
 * @description: Request list interface parameters
 */
export interface invSaveDetailInvParams {
  NOMOR_SERI_PAJAK: string;
  CONFIRM_N_SERI_PAJAK: string;
  TANGGAL_FAKTUR_PAJAK: string;
  NOMOR_INVOICE_SUPPLIER: string;
  TANGGAL_INVOICE_SUPPLIER: string;
  ID: number;
}

export interface invSaveDetailInvResult {
  id?: number;
}

/**
 * @description: Request list return value
 */
export type invSaveDetailInvResultModel = invSaveDetailInvResult;
