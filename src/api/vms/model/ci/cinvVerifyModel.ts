/**
 * @description: Request list interface parameters
 */
export interface CinvVerifyParams {
  id: string;
  // editor_name: string;
}

export interface CinvVerifyResult {
  i_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type CinvVerifyResultModel = CinvVerifyResult;
