/**
 * @description: Request list interface parameters
 */
export interface invInfoSuppSaveParams {
  ALAMAT_PERUSAHAAN: string;
  KODE_POS: number;
  KOTA: number;
  NEGARA: number;
  TELEPON: string;
  SC_ID: string;
}

export interface invInfoSuppSaveResult {
  sc_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type invInfoSuppSaveResultModel = invInfoSuppSaveResult;
