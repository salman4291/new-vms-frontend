/**
 * @description: Request list interface parameters
 */
export interface cinvInfoSuppSaveParams {
  NAMA_PERUSAHAAN: string;
  ALAMAT_PERUSAHAAN: string;
  KODE_POS: number;
  KOTA: number;
  NEGARA: number;
  NAMA: number;
  TELEPON: string;
  FAX: string;
  NPWP: string;
  EMAIL: string;
  I_ID: string;
  SC_ID: string;
}

export interface cinvInfoSuppSaveResult {
  i_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type cinvInfoSuppSaveResultModel = cinvInfoSuppSaveResult;
