import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type InvFileParams = BasicPageParams;
export type InvFileFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface CInvFileListItem {
  tax_invoice_pdf_file_name: string;
}

/**
 * @description: Request list return value
 */
export type CInvFileListGetResultModel = BasicFetchResult<CInvFileListItem>;
