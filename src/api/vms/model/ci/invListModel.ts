import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type InvParams = BasicPageParams;
export type InvFilterParams = BasicPageParams & { purchase_order?: string };

// TODO
export interface CInvListItem {
  i_id: string;
  referensi: string;
  merchant: string;
  store: string;
  revisi: string;
  tanggal_pfi: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type CInvListGetResultModel = BasicFetchResult<CInvListItem>;
