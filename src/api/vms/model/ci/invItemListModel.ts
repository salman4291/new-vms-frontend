/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type CInvItemListParams = BasicPageParams;

export interface CInvItemList {
  i_id: string;
  referensi: string;
  merchant: string;
  store: string;
  revisi: string;
  tanggal_pfi: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type CInvItemListGetResultModel = BasicFetchResult<CInvItemList>;
