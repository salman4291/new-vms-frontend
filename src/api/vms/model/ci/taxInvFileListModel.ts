import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type InvFileParams = BasicPageParams;
export type InvFileFilterParams = BasicPageParams & { i_id?: string };

// TODO
export interface CInvTaxFileListItem {
  seq: string;
  type: string;
  tax_invoice_pdf_file_name: string;  
}

/**
 * @description: Request list return value
 */
export type CInvTaxFileListGetResultModel = BasicFetchResult<CInvTaxFileListItem>;
