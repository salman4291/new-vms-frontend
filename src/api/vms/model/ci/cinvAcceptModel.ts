/**
 * @description: Request list interface parameters
 */
export interface CinvAcceptParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface CinvAcceptResult {
  i_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type CinvAcceptResultModel = CinvAcceptResult;
