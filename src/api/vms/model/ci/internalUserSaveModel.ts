/**
 * @description: Request list interface parameters
 */
export interface internalUserSaveParams {
  id: string;
  first_name: string;
  username: string;
  last_name: number;
  email: number;
  role: number;
  enabled: number;
  
}

export interface internalUserSaveResult {
  username?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type internalUserSaveResultModel = internalUserSaveResult;
