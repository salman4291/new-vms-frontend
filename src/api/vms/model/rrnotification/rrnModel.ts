import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type RrNParams = BasicPageParams;

// TODO
export interface RrNotifItem {
  id: string;
  return_request_number: string;
  request_created_date: string;
  supplier: string;
  notification: string;
  read_date: string;
  read_by: string;
  action_date: string;
  action_by: string;
  status: string;
  store: string;
}

/**
 * @description: Request list return value
 */
export type RrNotifListGetResultModel = BasicFetchResult<RrNotifItem>;
