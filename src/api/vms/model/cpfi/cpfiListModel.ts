import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type CPfiParams = BasicPageParams;
export type CPfiFilterParams = BasicPageParams & { cdt?: string };

// TODO
export interface CPfiListItem {
  show: boolean;
  referensi: string;
  merchant: string;
  store: string;
  revisi: string;
  tanggal_pfi: string;
  status: string;
  business_unit: string;
}

/**
 * @description: Request list return value
 */
export type CPfiListGetResultModel = BasicFetchResult<CPfiListItem>;
