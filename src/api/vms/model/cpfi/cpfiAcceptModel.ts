/**
 * @description: Request list interface parameters
 */
export interface CpfiAcceptParams {
  cdt: string;
  cdt2: string;
  // action: string;
  // editor_name: string;
}

export interface CpfiAcceptResult {
  cdt?: string;
  cdt2?: string;
  inv_id?: string;
}

/**
 * @description: Request list return value
 */
export type CpfiAcceptResultModel = CpfiAcceptResult;
