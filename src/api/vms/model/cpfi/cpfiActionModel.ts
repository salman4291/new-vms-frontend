/**
 * @description: Request list interface parameters
 */
export interface PfiActionParams {
  id: string;
  action: string;
  // editor_name: string;
}

export interface PfiActionResult {
  pfir_id?: string;
}

/**
 * @description: Request list return value
 */
export type PfiActionResultModel = PfiActionResult;
