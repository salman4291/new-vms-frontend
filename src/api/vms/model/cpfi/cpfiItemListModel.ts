/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type CpfiItemListParams = BasicPageParams;

export interface CPfiItemList {
  cdt: string;
  pfi_number: string;
  tanggal_pfi: string;
  revisi: string;
  store: string;
  department: string;
  nama_perusahaan: string;
  alamat_perusahaan: string;
  npwp: string;
  sales_start_date: string;
  sales_end_date: string;
  con_margin_rate: number;
  con_margin_value: string;
  supplier_code: string;
  supplier_name: string;
  supplier_phone: string;
  supplier_fax: string;
}

/**
 * @description: Request list return value
 */
export type CPfiItemListModel = BasicFetchResult<CPfiItemList>;
