/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type CreditNoteParams = BasicPageParams;

export interface CreditNoteItemList {
  id: number;
  supplier_code: string;
  return_number: string;
  return_date: string;
  business_unit: string;
  no_seri_pajak: number;
  tanggal_faktur_pajak: number;
  store: string;
  status: string;
}

/**
 * @description: Request list return value
 */
export type CreditNoteItemListModel = BasicFetchResult<CreditNoteItemList>;
