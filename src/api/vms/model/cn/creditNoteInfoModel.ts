/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type CnDetailParams = BasicPageParams & { id?: string };

export interface CnDetail {
  id: string;
  cn_id: string;
  no: string;
  barcode: string;
  nama_barang: string;
  total_qty: string;
  pajak: string;
  harga_unit: number;
  harga_sebelum_pajak: number;
  harga_sesudah_pajak: number;
  sub_total_return: number;
  ppn_return: number;
  total_amount: number;
}

/**
 * @description: Request list return value
 */
export type CnDetailGetResultModel = BasicFetchResult<CnDetail>;
