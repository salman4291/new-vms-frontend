/**
 * @description: Request list interface parameters
 */
export interface cnConfirmApiParams {
  id: string;
}

export interface cnConfirmApiResult {
  cn_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type cnConfirmApiResultModel = cnConfirmApiResult;
