/**
 * @description: Request list interface parameters
 */
export interface cnEditApiParams {
  id: string;
  tax_serial_number: string;
  tax_inv_date: string;
}

export interface cnEditApiResult {
  cn_id?: string;
  id?: string;
}

/**
 * @description: Request list return value
 */
export type cnEditApiResultModel = cnEditApiResult;
