/**
 * @description: Request list interface parameters
 */
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type CnDetailInfoParams = BasicPageParams & { id?: string };

export interface CnDetailInfo {
  id: string;
  tax_serial_number: string;
  tax_inv_date: string;
  return_number: string;
  nama_perusahaan: string;
  alamat_perusahaan: string;
  npwp: string;
  npwp_supplier: string;
  return_date: string;
  store_code: string;
  nama_perusahan_supplier: string;
  alamat_perusahaan_supplier: string;
  status: string;
}

/**
 * @description: Request list return value
 */
export type CnDetailInfoGetResultModel = BasicFetchResult<CnDetailInfo>;
