import { defHttp } from '/@/utils/http/axios';
import { DocParams, DocParams2, DocParams3, DocParams4, DocParams5, DocParams6, DocParams7, DocumentItemListModel, DocumentParams } from './model/documentberhubungan/ducumentModel';
import { DocReturParams, DocumentReturListModel } from './model/documentberhubungan/ducumentReturModel';
import { DocConsParams, DocumentCosnItemListModel } from './model/documentberhubungan/ducumentConsModel';

enum Api {
  DB_LIST = '/doc/getDocumentList',
  DR = '/doc/getDocumentRetur',
  DC = '/doc/getDocumentCons',
}

/**
 * @description: Get sample list value
 */

export const documentListApi = (params: DocParams) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const documentListApi2 = (params: DocParams2) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentListApi3 = (params: DocParams3) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentListApi4 = (params: DocParams4) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentListApi5 = (params: DocParams5) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentListApi6 = (params: DocParams6) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentListApi7 = (params: DocParams7) =>
  defHttp.get<DocumentItemListModel>({
    url: Api.DB_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentReturApi = (params: DocReturParams) =>
  defHttp.get<DocumentReturListModel>({
    url: Api.DR,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const documentConsApi = (params: DocConsParams) =>
  defHttp.get<DocumentCosnItemListModel>({
    url: Api.DC,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
