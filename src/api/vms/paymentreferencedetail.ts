import { defHttp } from '/@/utils/http/axios';
import { PrdParams, PrdListGetResultModel } from './model/prd/prdListModel';
// import { PfiItemListModel } from './model/pfi/pfiItemListModel';
import { PrdDetailInfoGetResultModel } from './model/prd/prdDetailInfoModel';
import { PrdItemListGetResultModel } from './model/prd/prdItemListModel';
// import { PfiActionParams, PfiActionResultModel } from './model/pfi/pfiActionModel';
// import { PfiRevertParams, PfiRevertResultModel } from './model/pfi/pfiRevertModel';

enum Api {
  PRD_LIST = '/prd/getPrdList',
  PRD_INFO = '/prd/viewbycdtinfo',
  PRD_DETAIL = '/prd/viewbycdtdetail',
}

/**
 * @description: Get sample list value
 */

export const prdListApi = (params: PrdParams) =>
  defHttp.get<PrdListGetResultModel>({
    url: Api.PRD_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const prdListApi2 = (params) =>
  defHttp.get<PrdListGetResultModel>({
    url: Api.PRD_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const prdInfoApi = (params) =>
  defHttp.post<PrdDetailInfoGetResultModel>({
    url: Api.PRD_INFO,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const prdDetailApi = (params) =>
  defHttp.post<PrdItemListGetResultModel>({
    url: Api.PRD_DETAIL,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
