import { UploadMsg } from './model/bpfr/bpfrUploadModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  UPLOAD = '/bpfr/uploadFile',
}

/**
 * @description: Get sample list value
 */
const formData = new FormData();

export const bpfrUploadApi = (params) =>
  defHttp.post<UploadMsg>({
    url: Api.UPLOAD,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: formData.append('namafile', params),
  });
