import { ArcDocListGetResultModel, ArcDocParams } from './model/adm/arcdocListModel';
import { BUListGetResultModel, BUParams } from './model/adm/businessUnit';
import { BusinessUnitSaveParams, BusinessUnitSaveResultModel } from './model/adm/businessUnitCreateModel';
import { InternalUserSaveParams, InternalUserSaveResultModel } from './model/adm/internalUserCreateModel';
import { internalUserSaveParams } from './model/adm/internalUserSaveModel';
import { StoreSaveParams, StoreSaveResultModel } from './model/adm/storeCreateModel';
import { IUListGetResultModel, IUListItem, IUListItem2, IUParams } from './model/adm/usrListModel';
import { WarehouseSaveParams, WarehouseSaveResultModel } from './model/adm/warehouseCreateModel';
import { WListGetResultModel, WParams } from './model/adm/warehouseListModel';
import { defHttp } from '/@/utils/http/axios';


enum Api {
  IU = '/adm/getIntUserList',
  IG = '/adm/getIntById',
  IUC = '/adm/createIntUser',
  IUU = '/adm/updateIntUser',
  DEPT = '/adm/getDepartmentList',
  STORE = '/adm/getStoreList',
  BU = '/adm/getBusinessUnitList',
  W = '/adm/getWarehouseList',
  AD = '/adm/getArcdocList',
  SC = '/adm/actionSave',
  PSL = '/adm/postStoreList',
  PSU = '/adm/postStoreUpdate',
  WC = '/adm/actionCreate',
  WG = '/adm/getWarehouseById',
  WU = '/adm/warehouseUpdate',
  BUC = '/adm/actionCreateBU',
  BUG = '/adm/getBusinessUnitById',
  BUU = '/adm/businessUnitUpdate',
}

/**
 * @description: Get sample list value
 */


export const BUListApi = (params: BUParams) =>
  defHttp.get<BUListGetResultModel>({
    url: Api.BU,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const IUListApi = (params: IUParams) =>
  defHttp.get<IUListGetResultModel>({
    url: Api.IU,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
  export const IUListApi3 = (params) =>
  defHttp.post<IUListGetResultModel>({
    url: Api.IG,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });
export const IUListApi2 = () =>
  defHttp.get<IUListGetResultModel>({
    url: Api.IU,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const WListApi = (params: WParams) =>
  defHttp.get<WListGetResultModel>({
    url: Api.W,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const ADListApi = (params: ArcDocParams) =>
  defHttp.get<ArcDocListGetResultModel>({
    url: Api.AD,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

  export const storeSaveApi = (params: StoreSaveParams) =>
  defHttp.post<StoreSaveResultModel>({
    url: Api.SC,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const postStoreApi = (params) =>
  defHttp.post<StoreSaveResultModel>({
    url: Api.PSL,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { store_id: params },
  });

  // export const storeUpdateApi = (params) =>
  // defHttp.post<StoreSaveResultModel>({
  //   url: Api.PSU,
  //   headers: {
  //     // @ts-ignore
  //     ignoreCancelToken: true,
  //   },
  //   data: { store_id: params },
  // });
  export const storeUpdateApi = (params: StoreSaveParams) =>
  defHttp.post<StoreSaveResultModel>({
    url: Api.PSU,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const warehouseCreateApi = (params: WarehouseSaveParams) =>
  defHttp.post<WarehouseSaveResultModel>({
    url: Api.WC,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const businessUnitCreateApi = (params: BusinessUnitSaveParams) =>
  defHttp.post<BusinessUnitSaveResultModel>({
    url: Api.BUC,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const postWarehouseApi = (params) =>
  defHttp.post<WarehouseSaveResultModel>({
    url: Api.WG,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

  export const warehouseUpdateApi = (params: WarehouseSaveParams) =>
  defHttp.post<WarehouseSaveResultModel>({
    url: Api.WU,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const postBusinessUnitApi = (params) =>
  defHttp.post<BusinessUnitSaveResultModel>({
    url: Api.BUG,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

  export const businessUnitUpdateApi = (params: BusinessUnitSaveParams) =>
  defHttp.post<BusinessUnitSaveResultModel>({
    url: Api.BUU,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const internalUserCreateApi = (params: InternalUserSaveParams) =>
  defHttp.post<InternalUserSaveResultModel>({
    url: Api.IUC,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const internalUserUpdateApi = (params: internalUserSaveParams) =>
  defHttp.post<InternalUserSaveResultModel>({
    url: Api.IUU,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });