import { defHttp } from '/@/utils/http/axios';
import { UsrListGetResultModel, UsrParams } from './model/usr/usrListModel';
import { DeptListGetResultModel, DeptParams } from './model/usr/deptListModel';
import { StoreListGetResultModel, StoreParams } from './model/usr/storeListModel';

enum Api {
  USERS = '/adm/getIntUserList',
  DEPT = '/adm/getDepartmentList',
  STORE = '/adm/getStoreList',
  BU = '/adm/getBusinessUnitList'
}

/**
 * @description: Get sample list value
 */

export const userListApi = (params: UsrParams) =>
  defHttp.get<UsrListGetResultModel>({
    url: Api.USERS,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const deptListApi = (params) =>
  defHttp.get<DeptListGetResultModel>({
    url: Api.DEPT,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const storeListApi = (params) =>
  defHttp.get<StoreListGetResultModel>({
    url: Api.STORE,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
