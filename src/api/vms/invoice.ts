import { defHttp } from '/@/utils/http/axios';
import { InvParams, InvListGetResultModel } from './model/inv/invListModel';
import { InvItemListGetResultModel } from './model/inv/invItemListModel';
import { InvDetailInfoGetResultModel } from './model/inv/invDetailInfoModel';
import {
  invInfoSuppSaveParams,
  invInfoSuppSaveResultModel,
} from './model/inv/invSaveInfoSuppModel';
import { invSaveDetailInvParams, invSaveDetailInvResultModel } from './model/inv/invSaveDetail';
import { InvFileListGetResultModel } from './model/inv/invFileListModel';
import { InvActionParams, InvActionResult, InvActionResultModel } from './model/inv/invActionModel';
import { InvFileTaxListGetResultModel } from './model/inv/invFileTaxModel';
import { InvCancelModel, InvCancelParams, InvCancelResult } from './model/inv/invCancelModel';
import { InvSaveItemDetailParams, InvSaveItemDetailResultModel } from './model/inv/invSaveItemDetailModel';
import { InvVerifyParams, InvVerifyResultModel } from './model/inv/invVerifyModel';

enum Api {
  INV_LIST = '/i/getInvoiceAll',
  INV_DETAIL_LIST = '/inv/invdetailsitem',
  INV_DETAIL_INFO = '/inv/invdetails',
  INV_SUPPLIER_SAVE = '/i/actionSaveInfoSupp',
  INV_DETAIL_SAVE = '/i/actionSaveDetailInv',
  TAX_INV = '/i/taxInvoiceFile',
  SUP_DOC_INV = '/i/supportDocFileInvoice',
  SUP_DOC_KWI = '/i/supportDocFileKwitansi',
  SUP_DOC_SJ = '/i/supportDocSuratJalan',
  UPLOAD_TAX_INV = '/i/uploadTaxInv',
  DEL_TAX = '/i/deleteTaxInv',
  DEL_DOC_INV = '/i/deleteInv',
  DEL_DOC_KWI = '/i/deleteKwitansi',
  DEL_DOC_SJ = '/i/deleteSuratJalan',
  ACTION_SEND = '/i/actionSend',
  ACTION_CANCEL = '/i/actionBack',
  ITEM_SAVE = '/i/actionSaveDetailItem',
  I_ACTION_REV = '/i/revisiDoc'

}

/**
 * @description: Get sample list value
 */

export const invListApi = (params: InvParams) =>
  defHttp.get<InvListGetResultModel>({
    url: Api.INV_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const invoDetListApi = (params) =>
  defHttp.post<InvItemListGetResultModel>({
    url: Api.INV_DETAIL_LIST,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const invInfoListApi = (params) =>
  defHttp.post<InvDetailInfoGetResultModel>({
    url: Api.INV_DETAIL_INFO,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const invSaveInfoSupp = (params: invInfoSuppSaveParams) =>
  defHttp.post<invInfoSuppSaveResultModel>({
    url: Api.INV_SUPPLIER_SAVE,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const invSaveDetailInvoice = (params: invSaveDetailInvParams) =>
  defHttp.post<invSaveDetailInvResultModel>({
    url: Api.INV_DETAIL_SAVE,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const invTaxListApi = (params) =>
  defHttp.post<InvFileTaxListGetResultModel>({
    url: Api.TAX_INV,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
    
  });
  export const invFileListApi = (params) =>
  defHttp.post<InvFileListGetResultModel>({
    url: Api.SUP_DOC_INV,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
    
  });
  export const invFileKListApi = (params) =>
  defHttp.post<InvFileListGetResultModel>({
    url: Api.SUP_DOC_KWI,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params  ,
    
  });
  export const invFileSJApi = (params) =>
  defHttp.post<InvFileListGetResultModel>({
    url: Api.SUP_DOC_SJ,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params  ,
    
  });

  export const invSendApi = (params:InvActionParams) =>
  defHttp.post<InvActionResult>({
    url: Api.ACTION_SEND,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const invActionApi = (params) =>
  defHttp.post<InvActionResultModel>({
    url: Api.DEL_DOC_INV,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const kwiActionApi = (params) =>
  defHttp.post<InvActionResultModel>({
    url: Api.DEL_DOC_KWI,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const sjActionApi = (params) =>
  defHttp.post<InvActionResultModel>({
    url: Api.DEL_DOC_SJ,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
  export const taxActionApi = (params) =>
  defHttp.post<InvActionResultModel>({
    url: Api.DEL_TAX,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const invoiceCancelApi = (params: InvCancelParams) =>
  defHttp.post<InvCancelResult>({
    url: Api.ACTION_CANCEL,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const invSaveItemDetailApi = (params: InvSaveItemDetailParams) =>
  defHttp.post<InvSaveItemDetailResultModel>({
    url: Api.ITEM_SAVE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

  export const invactionRevisiApi = (params: InvVerifyParams) =>
  defHttp.post<InvVerifyResultModel>({
    url: Api.I_ACTION_REV,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
