import { defHttp } from '/@/utils/http/axios';
import { RarParams, RarListGetResultModel } from './model/rar/rarListModel';
import { RarItemListModel } from './model/rar/rarItemListModel';
import { RarDetailInfoGetResultModel } from './model/rar/rarDetailInfoModel';
import { RarActionParams, RarActionResultModel } from './model/rar/rarActionModel';
import { RarRevertParams, RarRevertResultModel } from './model/rar/rarRevertModel';
import { RarSaveParams, RarSaveResultModel } from './model/rar/rarSaveModel';

enum Api {
  RAR_LIST = '/rar/getRarAll',
  RAR_DETAIL = `/rar/viewbycdtdetail`,
  RAR_INFO = '/rar/viewbycdtinfo',
  RAR_ACTION = '/rar/actionAcceptReject',
  RAR_REVERT = '/rar/actionRar',
  RAR_SAVE = '/rar/actionSave',
}
/**
 * @description: Get sample list value
 */

export const rarListApi = (params: RarParams) =>
  defHttp.get<RarListGetResultModel>({
    url: Api.RAR_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rarDetailApi = (params) =>
  defHttp.post<RarItemListModel>({
    url: Api.RAR_DETAIL,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rarInfoApi = (params) =>
  defHttp.post<RarDetailInfoGetResultModel>({
    url: Api.RAR_INFO,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rarActionApi = (params: RarActionParams) =>
  defHttp.post<RarActionResultModel>({
    url: Api.RAR_ACTION,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const rarRevertApi = (params: RarRevertParams) =>
  defHttp.post<RarRevertResultModel>({
    url: Api.RAR_REVERT,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const rarSaveApi = (params: RarSaveParams) =>
  defHttp.post<RarSaveResultModel>({
    url: Api.RAR_SAVE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
