import { defHttp } from '/@/utils/http/axios';
import { RcParams, RcListGetResultModel } from './model/rc/rcListModel';
import { RcDetailInfoGetResultModel } from './model/rc/rcDetailInfoModel';
import { RcItemListModel } from './model/rc/rcItemListModel';

enum Api {
  RC_LIST = '/rc/getReturnConfirmList',
  RC_DETAIL = '/rc/viewbycdtdetail',
  RC_INFO = '/rc/viewbycdtinfo',
}

/**
 * @description: Get sample list value
 */

export const rcListApi = (params: RcParams) =>
  defHttp.get<RcListGetResultModel>({
    url: Api.RC_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const rcDetailListApi = (params) =>
  defHttp.post<RcDetailInfoGetResultModel>({
    url: Api.RC_DETAIL,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const rcInfoListApi = (params) =>
  defHttp.post<RcItemListModel>({
    url: Api.RC_INFO,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });
