import { UploadMsg } from './model/bpfr/bpfrUploadModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  UPLOAD = '/i/uploadKwitansi',
}

/**
 * @description: Get sample list value
 */
const formData = new FormData();

export const invKwiUploadApi = (params) => {
	formData.append("namafile", params.file);
	formData.append("id", params.id);
	// formData.append("supp_code", params.supplier_code);

  defHttp.post({
    url: Api.UPLOAD,
		params: {
			id: params.id,
      supplier_code:params.supplier_code,
      purchase_order:params.purchase_order
			// supp_code: params.supplier_code,
		},
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: formData,
  });
}
