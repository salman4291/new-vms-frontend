import { CPfiDetailInfoGetResultModel } from './model/cpfi/cpfiDetailInfoModel';
import { CPfiItemListModel } from './model/cpfi/cpfiItemListModel';
import { CPfiListGetResultModel, CPfiParams } from './model/cpfi/cpfiListModel';
import { CSAListGetResultModel, CSAParams } from './model/csa/csaListModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  CSA_LIST = '/sa/getConsSalesAnalysisList',
}

/**
 * @description: Get sample list value
 */

export const csaListApi = (params) =>
  defHttp.get<CSAListGetResultModel>({
    url: Api.CSA_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

