import { defHttp } from '/@/utils/http/axios';
import { RaParams, RaListGetResultModel } from './model/ra/raListModel';
import { RaItemListModel } from './model/ra/raItemListModel';
import { RaDetailInfoGetResultModel } from './model/ra/raDetailInfoModel';
import { RaActionParams, RaActionGetResultModel } from './model/ra/raActionModel';

enum Api {
  RA_LIST = '/ra/getRaAll',
  RA_DETAIL = `/ra/viewbycdtdetail`,
  RA_INFO = '/ra/viewbycdtinfo',
  RA_ACTION = '/ra/actionRa',
}

/**
 * @description: Get sample list value
 */

export const raListApi = (params: RaParams) =>
  defHttp.get<RaListGetResultModel>({
    url: Api.RA_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const raDetailApi = (params) =>
  defHttp.post<RaItemListModel>({
    url: Api.RA_DETAIL,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const raInfoApi = (params) =>
  defHttp.post<RaDetailInfoGetResultModel>({
    url: Api.RA_INFO,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const raActionApi = (params: RaActionParams) =>
  defHttp.post<RaActionGetResultModel>({
    url: Api.RA_ACTION,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
