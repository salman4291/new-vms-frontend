import { CNrListGetResultModel, CNrParams } from './model/cnr/cnrListModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  CNR_LIST = '/nrc/getConsNotaReturList',
  CNR_DETAIL = `/cp/viewbycdtdetail`,
  CNR_INFO = '/cp/viewbycdtinfo',
	CNR_DOWNLOAD = '/nrc/getConsNotaReturDownload',
  CPFI_ACTION = '/pfi/actionPfi',
  CPFI_REVERT = '/pfi/actionBack',
}

/**
 * @description: Get sample list value
 */

export const cnrListApi = (params: CNrParams) =>
  defHttp.get<CNrListGetResultModel>({
    url: Api.CNR_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const cnrDownloadApi = (params) => {
	return defHttp.get({
    url: Api.CNR_DOWNLOAD,
    params,
    timeout: 10000,
  });
}

export const cpfiDetailApi = (params) =>
  defHttp.post<CPfiItemListModel>({
    url: Api.CPFI_DETAIL,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const cpfiInfoApi = (params) =>
  defHttp.post<CPfiDetailInfoGetResultModel>({
    url: Api.CPFI_INFO,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
