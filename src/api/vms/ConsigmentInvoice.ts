import { CinvAcceptParams, CinvAcceptResultModel } from './model/ci/cinvAcceptModel';
import {
  cinvInfoSuppSaveParams,
  cinvInfoSuppSaveResultModel,
} from './model/ci/cinvSaveInfoSuppModel';
import { CinvVerifyParams, CinvVerifyResultModel } from './model/ci/cinvVerifyModel';
import { CInvDetailInfoGetResultModel } from './model/ci/invDetailInfoModel';
import { CInvFileListGetResultModel, InvFileFilterParams } from './model/ci/invFileListModel';
import { CInvItemListGetResultModel, CInvItemListParams } from './model/ci/invItemListModel';
import {
  CInvListGetResultModel,
  InvFilterParams,
  InvListGetResultModel,
} from './model/ci/invListModel';
import { CInvTaxFileListGetResultModel } from './model/ci/taxInvFileListModel';
import {
  InvSaveItemDetailParams,
  InvSaveItemDetailResultModel,
} from './model/inv/invSaveItemDetailModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  CI_LIST = '/ci/getConsInvAll',
  CIT_LIST = '/ci/consTaxInvoiceFile',
  CIF_LIST = '/ci/consFileInvoice',
  CIK_LIST = '/ci/consFileKwitansi',
  CI_DETAIL = `/ci/consInvDetail`,
  CI_INFO = '/ci/consInvInfo',
  CI_ACTION = '/pfi/actionPfi',
  CI_REVERT = '/pfi/actionBack',
  CI_SUPPLIER_SAVE = '/ci/actionSaveInfoSupp',
  CI_ACTION_SEND = '/ci/actionSend',
  CI_ACTION_VER = '/ci/verifikasiDoc',
  CI_ACTION_REV = '/ci/revisiDoc',
  ITEM_SAVE = '/ci/actionSaveDetailItem',
}

/**
 * @description: Get sample list value
 */

export const cinvListApi = (params: InvFilterParams) =>
  defHttp.get<CInvListGetResultModel>({
    url: Api.CI_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const taxInvFileListApi = (params) =>
  defHttp.post<CInvTaxFileListGetResultModel>({
    url: Api.CIT_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
export const cinvFileListApi = (params) =>
  defHttp.post<CInvFileListGetResultModel>({
    url: Api.CIF_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
export const cinvFileKListApi = (params) =>
  defHttp.post<CInvFileListGetResultModel>({
    url: Api.CIK_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const cinvoDetListApi = (params) =>
  defHttp.post<CInvItemListGetResultModel>({
    url: Api.CI_DETAIL,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const cinvInfoListApi = (params) =>
  defHttp.post<CInvDetailInfoGetResultModel>({
    url: Api.CI_INFO,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const cinvSaveInfoSupp = (params: cinvInfoSuppSaveParams) =>
  defHttp.post<cinvInfoSuppSaveResultModel>({
    url: Api.CI_SUPPLIER_SAVE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const cinvactionAcceptApi = (params: CinvAcceptParams) =>
  defHttp.post<CinvAcceptResultModel>({
    url: Api.CI_ACTION_SEND,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const cinvactionVerifyApi = (params: CinvVerifyParams) =>
  defHttp.post<CinvVerifyResultModel>({
    url: Api.CI_ACTION_VER,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
export const cinvactionRevisiApi = (params: CinvVerifyParams) =>
  defHttp.post<CinvVerifyResultModel>({
    url: Api.CI_ACTION_REV,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const cinvSaveItemDetailApi = (params: InvSaveItemDetailParams) =>
  defHttp.post<InvSaveItemDetailResultModel>({
    url: Api.ITEM_SAVE,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
