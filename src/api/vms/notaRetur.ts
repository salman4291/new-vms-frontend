import { defHttp } from '/@/utils/http/axios';
import { NrParams, NrListGetResultModel } from './model/nr/nrListModel';
import { NrItemListModel } from './model/nr/nrItemListModel';
import { NrDetailInfoGetResultModel } from './model/nr/nrDetailInfoModel';

enum Api {
  NR_LIST = '/nr/getNrAll',
	NR_DOWNLOAD = '/nr/getNrDownload'
}

/**
 * @description: Get sample list value
 */

export const nrListApi = (params: NrParams) =>
  defHttp.get<NrListGetResultModel>({
    url: Api.NR_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
	
export const nrDownloadApi = (params) => {
	return defHttp.get({
		url: Api.NR_DOWNLOAD,
		params,
		timeout: 10000,
	});
}
	
