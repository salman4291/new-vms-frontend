import { defHttp } from '/@/utils/http/axios';
import { StoreListGetResultModel } from './model/dropdownOptions/storeListModel';
import { DepartmentListGetResultModel } from './model/dropdownOptions/departmentListModel';
import { BusinessUnitListGetResultModel } from './model/dropdownOptions/businessUnitListModel';
import { SupplierListGetResultModel } from './model/dropdownOptions/supplierListModel';
import { BusinessUnit2ListGetResultModel } from './model/dropdownOptions/businessUnit2ListModel';
import { RoleListGetResultModel } from './model/dropdownOptions/roleListModel';

enum Api {
  STORE_LIST = '/search/getStore',
  DEPARTMENT_LIST = '/search/getDepartment',
  BUSINESS_UNIT_LIST = '/search/getBusiness',
  BUSINESS_UNIT2_LIST = '/search/getBusiness2',
  SUPPLIER_LIST = '/search/getSupplier',
  ROLE_LIST = '/search/getRole',
}

export const storeListApi = () =>
  defHttp.get<StoreListGetResultModel>({
    url: Api.STORE_LIST,
    params: { limit: 1000000 },
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const departmentListApi = () =>
  defHttp.get<DepartmentListGetResultModel>({
    url: Api.DEPARTMENT_LIST,
    params: { limit: 1000000 },
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const businessUnitListApi = () =>
  defHttp.get<BusinessUnitListGetResultModel>({
    url: Api.BUSINESS_UNIT_LIST,
    params: { limit: 1000000 },
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const businessUnit2ListApi = () =>
  defHttp.get<BusinessUnit2ListGetResultModel>({
    url: Api.BUSINESS_UNIT2_LIST,
    params: { limit: 1000000 },
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const supplierListApi = () =>
  defHttp.get<SupplierListGetResultModel>({
    url: Api.SUPPLIER_LIST,
    params: { limit: 1000000 },
    timeout: 100000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
export const roleListApi = () =>
  defHttp.get<RoleListGetResultModel>({
    url: Api.ROLE_LIST,
    params: { limit: 1000000 },
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
