import { defHttp } from '/@/utils/http/axios';
import { CreditNoteItemListModel, CreditNoteParams } from './model/cn/creditNoteModel';
import { CnDetailInfoGetResultModel } from './model/cn/creditNoteDetailModel';
import { CnDetailGetResultModel } from './model/cn/creditNoteInfoModel';
import { cnEditApiParams, cnEditApiResultModel } from './model/cn/creditNoteEditModel';
import { cnConfirmApiParams, cnConfirmApiResultModel } from './model/cn/creditNoteConfirmModel';

enum Api {
  CN_LIST = '/cn/getCreditNoteList',
  CN_DETAIL_INFO = '/cn/viewbycdtinfo',
  CN_DETAIL = '/cn/viewbycdtdetail',
  CN_EDIT = '/cn/editCreditNode',
  CN_CONFIRM = '/cn/actionConfirm',
}

/**
 * @description: Get sample list value
 */

export const creditNoteListApi = (params: CreditNoteParams) =>
  defHttp.get<CreditNoteItemListModel>({
    url: Api.CN_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const creditNoteDetailInfoApi = (params) =>
  defHttp.post<CnDetailInfoGetResultModel>({
    url: Api.CN_DETAIL_INFO,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: { id: params },
  });

export const creditNoteDetailApi = (params) =>
  defHttp.post<CnDetailGetResultModel>({
    url: Api.CN_DETAIL,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    params,
  });

export const creditNoteEditApi = (params: cnEditApiParams) =>
  defHttp.post<cnEditApiResultModel>({
    url: Api.CN_EDIT,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });

export const creditNoteConfirmApi = (params: cnConfirmApiParams) =>
  defHttp.post<cnConfirmApiResultModel>({
    url: Api.CN_CONFIRM,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: params,
  });
