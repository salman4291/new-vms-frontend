import { defHttp } from '/@/utils/http/axios';
import { PoParams, PoListGetResultModel } from './model/po/poListModel';
import { PoItemParams, PoItemListGetResultModel } from './model/po/poItemListModel';
import { PoDetailInfoParams, PoDetailInfoGetResultModel } from './model/po/poDetailInfoModel';

enum Api {
  PO_LIST = '/po/getPoAll',
  AdvancePo = '/po/advanceSearchPO',
  PO_ITEM_LIST = `/po/viewbycdtdetail`,
  // PO_ORDER_INFO = `/purchase-order/getPoOrderInfo`,
  // PO_DETAIL_INFO = `/po/viewbycdtdetail`,
  PO_DETAIL_INFO = `/po/viewbycdtinfo`,
}

/**
 * @description: Get sample list value
 */

export const poListApi = (params: PoParams) =>
  defHttp.get<PoListGetResultModel>({
    url: Api.PO_LIST,
    params,
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const poItemListApi = (params: PoItemParams) =>
  defHttp.get<PoItemListGetResultModel>({
    url: Api.PO_ITEM_LIST,
    timeout: 50000,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const poDetailInfoApi = (params: PoDetailInfoParams) =>
  defHttp.post<PoDetailInfoGetResultModel>({
    url: Api.PO_DETAIL_INFO,
    timeout: 50000,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });
