import { UploadMsg } from './model/bpfr/bpfrUploadModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  UPLOAD = '/ef/uploadEFaktur',
}

/**
 * @description: Get sample list value
 */
const formData = new FormData();

export const efakturUploadApi = (params) => {
  formData.append('namafile', params.file);
  formData.append('taxnumber', params.tax);
  formData.append('supplier_code', params.supplier_code);

  defHttp.post({
    url: Api.UPLOAD,
    params: {
      taxnumber: params.tax,
      supplier_code: params.supplier_code,
    },
    timeout: 50000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
    data: formData,
  });
};
