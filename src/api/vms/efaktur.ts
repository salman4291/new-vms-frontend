import { defHttp } from '/@/utils/http/axios';
import { efParams, EfListGetResultModel } from './model/ef/efListModel';

enum Api {
  EF_LIST = '/ef/getEFakturList',
  EF_DOWNLOAD = '/ef/download',
}

/**
 * @description: Get sample list value
 */

export const efListApi = (params: efParams) =>
  defHttp.get<EfListGetResultModel>({
    url: Api.EF_LIST,
    params,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

export const efDownload = (params) =>
  defHttp.post({
    url: Api.EF_DOWNLOAD,
    timeout: 10000,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
		data: params,
  });
