import type { AppRouteModule } from '/@/router/types';

import { t } from '/@/hooks/web/useI18n';
import { LAYOUT } from '/@/router/constant';
import addIndex from './layout-menu/indexMenu';
// import addChild from './layout-menu/childMenu';
import addDetail from './layout-menu/detailMenu';
import { RoleEnum } from '/@/enums/roleEnum';

const PurchaseOrder: AppRouteModule = {
  path: '/purchase-order',
  name: 'PurchaseOrder',
  component: LAYOUT,
  redirect: '/purchase-order/index',
  meta: {
    orderNo: 15,
    icon: 'vms-purchase-order|svg',
    hideChildrenInMenu: true,
    title: t('Purchase Order'),
    roles:[
      'Admin IT Role',
      'Admin Support Role',
      'General Manager',
      'Store Manager',
      'Senior Buyer Role',
      'Receiving Head Role',
      'APD Role',
      'Read Only Role',
      'Mechandiser',
      'SUPPLIER'
    ]
  },
  children: [
    addIndex('index', 'PurchaseOrder', 'Purchase Order'),
    addDetail('detail/:cdt', 'PurchaseOrderDetail', `PO Detail`),
  ],
};

export default PurchaseOrder;
