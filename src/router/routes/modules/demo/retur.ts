import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
import addChild from './layout-menu/childMenu';
import addDetail from './layout-menu/detailMenu';
import addDetailfolder from './layout-menu/detailFolderMenu';

const RETUR_PREFIX = 'RET';

const retur: AppRouteModule = {
  path: '/retur',
  name: 'Retur',
  component: LAYOUT,
  redirect: '/retur',
  meta: {
    orderNo: 33,
    icon: 'vms-retur|svg',
    title: t('Retur'),
    roles:[
      'Admin IT Role',
      'Admin Support Role',
      'Central Returns',
      'AR Role',
      'Receiving Head Role',
      'APD Role',
      'SUPPLIER'
    ]
  },
  children: [
    addChild('request', 'ReturRequest', 'Retur Request', RETUR_PREFIX),
    addDetailfolder(
      'request/detail/:id',
      'ReturRequestDetail',
      'Retur Request Detail',
      RETUR_PREFIX,
    ),
    addChild('request-response', 'ReturRequestResponse', 'Retur Request Response', RETUR_PREFIX),
    addDetailfolder(
      'request-response/detail/:id',
      'ReturRequestResponseDetail',
      'Retur Request Response Detail',
      RETUR_PREFIX,
    ),
    addChild('confirmation', 'ReturConfirmation', 'Retur Confirmation', RETUR_PREFIX),
    addDetailfolder(
      'confirmation/detail/:id',
      'ReturConfirmationDetail',
      'Retur Confirmation Detail',
      RETUR_PREFIX,
    ),
    addChild(
      'confirmation?status=CLOSED',
      'ClosedReturConfirmation',
      'Closed Retur Confirmation',
      RETUR_PREFIX,
    ),
    addChild(
      'request-notification',
      'ReturRequestNotification',
      'Retur Request Notification',
      RETUR_PREFIX,
    ),
    addDetailfolder(
      'request-notification/detail/:id',
      'ReturRequestNotificationDetail',
      'Retur Request Notification Detail',
      RETUR_PREFIX,
    ),
    // addChild(
    //   'confirmation-notification',
    //   'ReturConfirmationNotification',
    //   'Retur Confirmation Notification',
    //   RETUR_PREFIX,
    // ),
  ],
};

export default retur;
