import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
import addChild from './layout-menu/childMenu';
import addDetail from './layout-menu/detailMenu';
import addDetailfolder from './layout-menu/detailFolderMenu';
import { RoleEnum } from '/@/enums/roleEnum';
import { ROLES_KEY } from '/@/enums/cacheEnum';
import path from 'path';

const CONSIGNMENT_PREFIX = 'CSG';

const consignment: AppRouteModule = {
  path: '/consignment',
  name: 'Consignment',
  component: LAYOUT,
  redirect: '/consignment',
  meta: {
    orderNo: 23,
    icon: 'vms-consignment|svg',
    title: t('Consignment'),
    roles:[
      'Admin IT Role',
      'Admin Support Role',
      'General Manager',
      'Store Manager',
      'Senior Buyer Role',
      'Receiving Head Role',
      'APD Role',
      'Read Only Role',
      'Mechandiser',
      'SUPPLIER',
      'Supplier Role'
    ]
  },
  children: [
    addChild('nota_retur', 'ConsignmentNotaRetur', 'Consignment Nota Retur', CONSIGNMENT_PREFIX),
    addChild('pfi', 'ConsignmentPFI', 'Consignment PFI', CONSIGNMENT_PREFIX),
    addDetailfolder(
      'pfi/detail/:id',
      'ConsignmentPFIDetail',
      'Consignment PFI Detail',
      CONSIGNMENT_PREFIX,
    ),
    // addDetail('detail/:referensi', 'ConsignmentPFIDetail', 'Consignment PFI Detail'),
    addChild('invoice', 'ConsignmentInvoice', 'Consignment Invoice', CONSIGNMENT_PREFIX),
    addDetailfolder(
      'invoice/detail/:id',
      'ConsignmentInvoiceDetail',
      'Consignment Invoice Detail',
      CONSIGNMENT_PREFIX,
    ),
    addChild('sales-analysis', 'SalesAnalysis', 'Sales Analysis', CONSIGNMENT_PREFIX),
    addChild(
      'sales-litigation-analysis',
      'SalesLitigationAnalysis',
      'Sales Litigation Analysis',
      CONSIGNMENT_PREFIX,
    ),
    addDetailfolder(
      'sales-litigation-analysis/detail/:id',
      'SalesLitigationAnalysisDetail',
      'Sales Litigation Analysis Detail',
      CONSIGNMENT_PREFIX,
    ),
    // addDetail('pfi/detail/:cdt', 'ConsignmentPFIDetail', `Consignment PFI Detail`),
  ],
};

export default consignment;
