import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
import addChild from './layout-menu/childMenu';

const EFAKTUR_PREFIX = 'EFA';

const EFaktur: AppRouteModule = {
  path: '/e-faktur',
  name: 'E-Faktur',
  component: LAYOUT,
  redirect: '/e-faktur',
  meta: {
    orderNo: 23,
    icon: 'vms-e-faktur|svg',
    title: t('E-Faktur'),
    roles:[
      'Admin IT Role',
      'Admin Support Role',
      'AR Role',
      'SUPPLIER'
    ]
  },
  children: [
    addChild('index', 'EFaktur', 'E-Faktur', EFAKTUR_PREFIX),
    addChild('upload', 'UploadEFaktur', 'Upload E-Faktur', EFAKTUR_PREFIX),
  ],
};

export default EFaktur;


// import type { AppRouteModule } from '/@/router/types';

// import { LAYOUT } from '/@/router/constant';
// import { t } from '/@/hooks/web/useI18n';
// import addChild from './layout-menu/childMenu';

// const EFAKTUR_PREFIX = 'EFA';

// const EFaktur: AppRouteModule = {
//   path: '/e-faktur',
//   name: 'EFaktur',
//   component: LAYOUT,
//   redirect: '/e-faktur',
//   meta: {
//     orderNo: 25,
//     icon: 'vms-e-faktur|svg',
//     title: t('E-Faktur'),
//     roles:[
//       'Admin IT Role',
//       'Admin Support Role',
//       'AR Role',
//       'SUPPLIER'
//     ]
//   },
//   children: [
//     addChild('index', 'EFaktur', 'E-Faktur', EFAKTUR_PREFIX),
//   ],
// };

// export default EFaktur;
