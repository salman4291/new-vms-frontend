import type { AppRouteModule } from '/@/router/types';

import { t } from '/@/hooks/web/useI18n';
import { LAYOUT } from '/@/router/constant';
import addIndex from './layout-menu/indexMenu';
import { RoleEnum } from '/@/enums/roleEnum';

const Dashboard: AppRouteModule = {
  path: '/dashboard',
  name: 'Dashboard',
  component: LAYOUT,
  redirect: '/dashboard/index',
  meta: {
    orderNo: 1,
    icon: 'material-symbols:computer-outline-rounded',
    title: t('Dashboard'),
    hideChildrenInMenu: true,
    roles: [
      RoleEnum.ROLE_ADMIN,
      RoleEnum.ROLE_ADMIN_SUPPORT,
      RoleEnum.ROLE_APD,
      RoleEnum.ROLE_LOCAL_BUYER,
      RoleEnum.ROLE_SRBUYER,
      RoleEnum.ROLE_STORE_MGR,
      RoleEnum.ROLE_TREASURY,
      RoleEnum.ROLE_TREASURY_MANAGER,
      RoleEnum.ROLE_SUPPLIER,
      RoleEnum.ROLE_SUPPLIER_ADMIN,
      RoleEnum.ROLE_APD,
      RoleEnum.ROLE_APD_RECEIVER,
      RoleEnum.ROLE_RECEIVING_HEAD,
      RoleEnum.ROLE_FWS_USER,
      RoleEnum.ROLE_BUYER_MGR,
      RoleEnum.ROLE_ADMIN_SUPPORT,
      RoleEnum.ROLE_INVOICE_VIEW,
      RoleEnum.ROLE_SUPPLIER_PRINCIPAL,
      RoleEnum.ROLE_SUPPLIER_DISTRIBUTOR,
      RoleEnum.ROLE_STORE_MANAGER_2,
      RoleEnum.ROLE_CR,
      RoleEnum.ROLE_GM,
      RoleEnum.SUPER,
      RoleEnum.SUPPLIER,
      RoleEnum.SUPPORT,
    ],
  },
  children: [addIndex('index', 'Dashboard', 'Dashboard')],
};

export default Dashboard;
