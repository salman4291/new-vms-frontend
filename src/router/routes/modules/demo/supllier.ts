import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
import addChild from './layout-menu/childMenu';
import addDetailfolder from './layout-menu/detailFolderMenu';

const SUPPLIER_PREFIX = 'SUP';

const supplier: AppRouteModule = {
  path: '/supplier',
  name: 'Supplier',
  component: LAYOUT,
  meta: {
    orderNo: 13,
    icon: 'vms-supplier|svg',
    title: t('Supplier'),
    roles: [
      'Admin IT Role',
      'Admin Support Role',
    ],
  },
  // component: () => import('/@/views/demo/charts/map/Baidu.vue'),
  children: [
    addChild('master-detail', 'SupplierMasterDetail', 'Supplier Master Detail', SUPPLIER_PREFIX),
    addChild('registered', 'RegisteredSupplier', 'Registered Supplier', SUPPLIER_PREFIX),
    addDetailfolder(
      'registered/detail/:id',
      'RegisteredSupplierDetail',
      'Supplier Registered Detail',
      SUPPLIER_PREFIX,
    ),
    addChild('create', 'CreateSupplier', 'Create Supplier', SUPPLIER_PREFIX),
    addChild('users', 'SupplierUsers', 'SupplierUsers', SUPPLIER_PREFIX),
    addDetailfolder('users/edit/:id', 'SupplierUserEdit', 'Supplier User Edit', SUPPLIER_PREFIX),
  ],
};

export default supplier;
