import type { AppRouteModule } from '/@/router/types';

import { t } from '/@/hooks/web/useI18n';
import { LAYOUT } from '/@/router/constant';
import addIndex from './layout-menu/indexMenu';
import addDetail from './layout-menu/detailMenu';

const PaymentReferenceDetail: AppRouteModule = {
  path: '/prd', //written as prh
  name: 'PaymentReferenceDetail',
  component: LAYOUT,
  redirect: '/prd/index',
  meta: {
    orderNo: 24,
    icon: 'vms-payment-reference|svg',
    hideChildrenInMenu: true,
    title: t('Payment Reference Detail'),
    roles: ['SUPPLIER','Admin IT Role'],
  },
  children: [
    addIndex('index', 'PaymentReferenceDetail', 'Payment Reference Detail'),
    addDetail('detail/:cdt', 'PRDDetail', `Payment Reference Detail`),
  ],
};

export default PaymentReferenceDetail;
