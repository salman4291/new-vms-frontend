import type { AppRouteRecordRaw } from '/@/router/types';
import addLayout from './layoutMenu';
import { RoleEnum } from '/@/enums/roleEnum';

function addDetailfolder(
  path: string,
  name: string,
  title: string,
  componentPrefix?: string,
  children?: AppRouteRecordRaw[],
): AppRouteRecordRaw {
  const component = () => {
    return componentPrefix != undefined
      ? import(/* @vite-ignore */ `/@/views/components/${componentPrefix}/${name}.vue`)
      : undefined;
  };
  return addLayout(path, name, title, component, children, true);
}

export default addDetailfolder;
