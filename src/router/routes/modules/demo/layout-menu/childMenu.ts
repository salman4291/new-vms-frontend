import type { AppRouteRecordRaw } from '/@/router/types';
import addLayout from './layoutMenu';
import { RoleEnum } from '/@/enums/roleEnum';

function addChild(
  path: string,
  name: string,
  title: string,
  componentPrefix?: string,
  children?: AppRouteRecordRaw[],
  // roles: RoleEnum[],
): AppRouteRecordRaw {
  const component = () => {
    return componentPrefix != undefined
      ? import(/* @vite-ignore */ `/@/views/components/${componentPrefix}/${name}.vue`)
      : undefined;
  };
  return addLayout(path, name, title, component, children);
}

export default addChild;
