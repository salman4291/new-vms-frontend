import {
  storeListApi,
  departmentListApi,
  businessUnitListApi,
  supplierListApi,
  businessUnit2ListApi,
  roleListApi,
} from '/@/api/vms/main';
import { StoreListGetResultModel } from '/@/api/vms/model/dropdownOptions/storeListModel';
import { Store2ListGetResultModel } from '/@/api/vms/model/dropdownOptions/store2ListModel';
import { DepartmentListGetResultModel } from '/@/api/vms/model/dropdownOptions/departmentListModel';
import { BusinessUnitListGetResultModel } from '/@/api/vms/model/dropdownOptions/businessUnitListModel';
import { SupplierListGetResultModel } from '/@/api/vms/model/dropdownOptions/supplierListModel';
import { BusinessUnit2ListGetResultModel } from '/@/api/vms/model/dropdownOptions/businessUnit2ListModel';
import { RoleListGetResultModel } from '/@/api/vms/model/dropdownOptions/roleListModel';

const storeListApiResult: StoreListGetResultModel = await storeListApi();
const storeList: any = { '': 'All' };
storeListApiResult.items.forEach(
  (store) => (storeList[store.store_id] = `${store.store_id} - ${store.name}`),
);
// const storeList = {}; //temporary
export const STORE_LIST = storeList;

const departmentListApiResult: DepartmentListGetResultModel = await departmentListApi();
const departmentList: any = { '': 'All' };
departmentListApiResult.items.forEach(
  (dept) => (departmentList[dept.code_id] = `${dept.code_id} - ${dept.name}`),
);
// const departmentList = {}; //temporary
export const DEPARTMENT_LIST = departmentList;

const businessUnitListApiResult: BusinessUnitListGetResultModel = await businessUnitListApi();
const businessUnitList: any = { '': 'All' };
businessUnitListApiResult.items.forEach(
  (bu) =>
    (businessUnitList[
      bu.business_unit_code
    ] = `${bu.business_unit_code} - ${bu.business_unit_name}`),
);
// const businessUnitList = {}; //temporary
export const BUSINESS_UNIT_LIST = businessUnitList;

const businessUnit2ListApiResult: BusinessUnit2ListGetResultModel = await businessUnit2ListApi();
const businessUnit2List: any = { '': 'All' };
businessUnit2ListApiResult.items.forEach(
  (bu) =>
    [
      (businessUnit2List[bu.id] = `${bu.business_unit_code} - ${bu.business_unit_name}`),
    ],
);
// const businessUnitList = {}; //temporary
export const BUSINESS_UNIT2_LIST = businessUnit2List;

const supplierListApiResult: SupplierListGetResultModel = await supplierListApi();
const supplierList: any = { '': 'All' };
supplierListApiResult.items.forEach(
  (supp) => (supplierList[supp.code] = `${supp.code} - ${supp.name}`),
);
// const businessUnitList = {}; //temporary
export const SUPPLIER_LIST = supplierList;

const roleListApiResult: RoleListGetResultModel = await roleListApi();
const roleList: any = { '': 'All' };
roleListApiResult.items.forEach(
  (supp) => (roleList[supp.id] = `${supp.id} - ${supp.name}`),
);
// const businessUnitList = {}; //temporary
export const ROLE_LIST = roleList;

//hard-coded; unused
// export const SUPPLIER_LIST = {
//   '': 'All',
//   '0050': '0050 - JKT (WINGS) SAYAP MAS UTAMA, PT',
//   '0059': '0059 - PT.UNILEVER INDONESIA',
//   '0142': '0142 - JKT(SARI ROTI) NIPPON INDOSARI CORP',
//   '0250': '0250 - PRESINDO CENTRAL PT',
//   '0403': '0403 - BHAKTI IDOLA TAMA, PT',
//   '0412': '0412 - SAMSUNG ELECTRONICS INDONESIA, PT',
//   '0452': '0452 - PT.RICKY PUTRA GLOBALINDO Tbk',
//   '0720': '0720 - JKT(DIAMOND)SUKANDA DJAYA (FRESHML)',
//   '1433': '1433 - SARANA KENCANA MULYA,PT',
//   '1609': '1609 - PT.SO GOOD FOOD',
//   '2284': '2284 - JKT(FIESTA)PRIMAFOOD INT. P',
//   '2459': '2459 - PT ENSEVAL PUTERA MEGATRADING Tbk',
//   '2897': '2897 - JKT CITRA DIMENSI ARTHALI',
//   '3052': '3052 - JKT(BENDERA MILK) FRISIAN FLAG IND.',
//   '3279': '3279 - JKT INDOGUNA UTAMA PT',
//   '3400': '3400 - SBY(KHONG GUAN)PT PUJI SURYA INDAH',
//   '3560': '3560 - SBY (MARJAN) PT PUJI SURYA INDAH',
//   '3654': '3654 - SBY(BENDERA MILK) FRISIAN FLAG IND.',
//   '3854': '3854 - PT DOS NI ROHA',
//   '4282': '4282 - JKT(TROPICANA SLIM)NUTRIFOOD I.',
//   '4895': '4895 - UD.JAYA MAKMUR',
//   '5384': '5384 - JKT(SHINZUI)FOCUS DISTRIBUSI IND.',
//   '5497': '5497 - BPK HENGKI',
//   '6050': '6050 - JKT SHARP ELECTRONICS INDONESIA',
//   '6767': '6767 - BUMI LANCAR, CV',
//   '7398': '7398 - YOG(WINGS)CIPTA KARYA AGUNG ABADI',
//   '8177': '8177 - YOG(ROMA)K33 DISTRIBUSI PT',
//   '8671': '8671 - JKT(GARUDA) SINAR NIAGA SEJAHTERA',
//   A185: 'A185 - PT.ARTA BOGA CEMERLANG',
//   AA18: 'AA18 - PT.EVERBRIGHT',
//   B805: 'B805 - SLO (MAYORA) TRIDAYA SUMBER REJEKI',
//   G868: 'G868 - JKT (SASA) TUMBAKMAS NIAGASAKTI PT',
//   H849: 'H849 - SAMSUNG ELECTRONICS INDONESIA-MTT',
//   I397: 'I397 - JKT (MARCKS) ANUGERAH PHARMINDO LESTARI',
//   I867: 'I867 - PT ARTA BOGA CEMERLANG',
//   K675: 'K675 - PANCADHARMA CENTRABHAKTI, PT / CONS',
//   K800: 'K800 - LIE ALI GUNAWAN',
//   K864: 'K864 - PT.TARGET MAKMUR SENTOSA',
//   L823: 'L823 - JKT NINY',
//   M309: 'M309 - JKT (ABC MIE) SARANA ABADI MAKMUR BERSAM',
//   M673: 'M673 - JKT (BALI DANCER) MENARA FOOD PT',
//   M813: 'M813 - PT.SIMPATINDO MULTI MEDIA',
//   N318: 'N318 - JKT (PASEO) THE UNIVENUS PT',
//   N329: 'N329 - CRB (DUA KELINCI) PAMER PT',
//   N809: 'N809 - JKT AGRO BOGA UTAMA, PT',
//   N852: 'N852 - JKT UNILEVER FOOD SOLUTIONS',
//   O025: 'O025 - PT. AUSTASIA FOOD',
//   O423: 'O423 - PT. RANCANG INDAH SENTOSA',
//   O608: 'O608 - CV. CONSINA SEGARA ALAM',
//   O870: 'O870 - PT. MENSA BINASUKSES',
//   O903: 'O903 - PT. VARIA KENCANA',
//   P113: 'P113 - PT. INSAN CITRAPRIMA SEJAHTERA',
//   P287: 'P287 - PT. TARGET MAKMUR SENTOSA',
//   P973: 'P973 - PT. BERLIAN INTI MAKMUR',
//   Q023: 'Q023 - IMAS MAESAROH',
//   Q611: 'Q611 - PT. GRIFF PRIMA ABADI',
//   Q926: 'Q926 - CV. ELATINDO KHARISMA JAYA',
//   Q945: 'Q945 - PT. DHARMA KARSA UTAMA',
//   S034: 'S034 - PT. SINARMAS DISTRIBUSI NUSANTARA',
//   S392: 'S392 - PT. PARIT PADANG GLOBAL',
//   S512: 'S512 - PT. KASIH KARUNIA SEJATI',
//   S650: 'S650 - IRIANTO GUNAWAN',
//   S892: 'S892 - PT. AEKTA MANDIRI KREASI',
//   S947: 'S947 - PT. HIDAYAH INSAN MULIA',
//   T011: 'T011 - CV. RABBANI ASYSA',
//   T795: 'T795 - PT INDOMARCO ADI PRIMA',
//   U214: 'U214 - PT CAHAYA AGUNG CEMERLANG',
//   U616: 'U616 - PT ANUGERAH PHARMINDO LESTARI',
//   U694: 'U694 - PT SARANA ABADI MAKMUR BERSAMA',
//   U871: 'U871 - PT INDOMARCO ADI PRIMA',
//   V403: 'V403 - PT LAMPUNG DISTRIBUSINDO RAYA',
//   V723: 'V723 - PT OTSUKA DISTRIBUTION INDONESIA',
//   V910: 'V910 - PT SYNNEX METRODATA INDONESIA',
//   W031: 'W031 - PT INDOFRESH',
//   W086: 'W086 - PT YELLO MUDA BERKAH SEJAHTERA',
//   W275: 'W275 - PT PINUS MERAH ABADI',
//   W278: 'W278 - PT PINUS MERAH ABADI',
//   W295: 'W295 - PT KLASIK DISTRIBUSI INDONESIA',
//   X008: 'X008 - CV CAHAYA PUTRI BINTANG',
//   X218: 'X218 - PT ANUGERAH INDOTIRTA RAHARJA',
//   X298: 'X298 - MUHAMMAD SARWANI',
//   X333: 'X333 - PT TAS CENTRE CEMERLANG',
//   X663: 'X663 - PT HEINZ ABC INDONESIA',
//   X750: 'X750 - PT SURYA LANCAR MAKMUR',
//   X957: 'X957 - PT WIDODO MAKMUR UNGGAS',
//   Y300: 'Y300 - PT MEKAR NIAGA SENTOSA',
//   Y443: 'Y443 - PT PANEL INDOFURN',
//   Y671: 'Y671 - YUDI FADLY',
//   Z419: 'Z419 - PT SIMPATINDO MULTI MEDIA ',
//   Z472: 'Z472 - CV INDO NICO PUTRA',
//   Z480: 'Z480 - PT PANGKAL PINANG DISTRIBUSINDORAYA',
//   Z485: 'Z485 - PT INOCYCLE TECHNOLOGY GROUP TBK',
//   Z559: 'Z559 - CV INDONICO PUTRA',
//   Z711: 'Z711 - CV INDONICO PUTRA',
//   Z712: 'Z712 - CV INDONICO PUTRA',
//   Z714: 'Z714 - PT. ELANG UTAMA KARYA',
//   Z718: 'Z718 - PT KERJA UNTUK INDONESIA',
//   Z813: 'Z813 - PT INTI KATAMATA',
//   Z814: 'Z814 - PT OPTICAL PARTNERS INDONESIA',
//   Z990: 'Z990 - PD NAINA EXIMINDO',
// };
