export enum RoleEnum {
  ROLE_LOCAL_BUYER = 'Local Buyer Role',
  ROLE_ADMIN = 'Admin IT Role',
  ROLE_SRBUYER = 'Mechandiser',
  ROLE_STORE_MGR = 'Read Only Role',
  ROLE_TREASURY = 'Treasury Role',
  ROLE_TREASURY_MANAGER = 'Treasury Manager Role',
  ROLE_SUPPLIER = 'Supplier Role',
  ROLE_SUPPLIER_ADMIN = 'Supplier Admin Role',
  ROLE_APD = 'APD Role',
  ROLE_APD_RECEIVER = 'APD Receiver Role',
  ROLE_RECEIVING_HEAD = 'Receiving Head Role',
  ROLE_FWS_USER = 'FWS User Role',
  ROLE_BUYER_MGR = 'Senior Buyer Role',
  ROLE_ADMIN_SUPPORT = 'Admin Support Role',
  ROLE_INVOICE_VIEW = 'Invoice Viewer',
  ROLE_SUPPLIER_PRINCIPAL = 'Supplier Principal Role',
  ROLE_SUPPLIER_DISTRIBUTOR = 'Supplier Distributor Role',
  ROLE_ARD = 'AR Role',
  ROLE_STORE_MANAGER_2 = 'Store Manager',
  ROLE_CR = 'Central Returns',
  ROLE_GM = 'General Manager',

  // super admin
  SUPER = 'super',

  // tester
  TEST = 'test',

  // for delivery
  SUPPLIER = 'SUPPLIER',
  SUPPORT = 'IT SUPPORT',
}
