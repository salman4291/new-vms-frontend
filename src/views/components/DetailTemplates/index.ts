export function toFixedFloat(num: number): string {
  return num.toLocaleString('en-US', {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });
}

export function toNumberFloat(num: string): number {
  return Number.parseFloat(num);
}

export function toDateFormat(dateStr: string): string {
  const date = new Date(dateStr);
  const formatter = new Intl.DateTimeFormat('en-US', {
    weekday: 'short',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    hour12: false,
    timeZone: 'GMT',
  });
  return formatter.format(date);
}

// Changing "DD-MM-YYYY HH-mm-ss" to "YYYY-MM-DD" for API params
export function reformatDate(dateStr: string): string {
  dateStr = dateStr.slice(0, 10);
  return dateStr.split('-').reverse().join('-');
}

import { withInstall } from '/@/utils';
import itemCodeCell from './src/ItemCodeCell.vue';
import status from './src/Status.vue';
import barcodeBankPromoSalesType from './src/BarcodeBankPromoSalesType.vue';
import field from './src/Field.vue';
import remark from './src/Remark.vue';
import itemNameCell from './src/itemNameCell.vue';
import quantityOrderCell from './src/QtyOrderCell.vue';
import dropDocumentItem from './src/DropDocumentItem.vue';
import dropDocumentRetur from './src/DropDocumentRetur.vue';
import documentDropdownButton from './src/DocumentDropdownButton.vue';
import documentReturDropdownButton from './src/DocumentReturDropdownButton.vue';
import poDetailMain from './src/po/DetailMain.vue';
import raDetailMain from './src/ra/DetailMain.vue';
import rarDetailSupport from './src/rar/DetailSupport.vue';
import rarDetailSupplier from './src/rar/DetailSupplier.vue';
import pfiDetailMain from './src/pfi/DetailMain.vue';
import pfirDetailSupplier from './src/pfir/DetailSupplier.vue';
import invDetailMain from './src/inv/DetailMain.vue';
//retur
import rrDetailMain from './src/retur/rr/DetailMain.vue';
import rrnDetailMain from './src/retur/rrn/DetailMain.vue';
import rrrDetailMain from './src/retur/rrr/DetailMain.vue';
import rcDetailMain from './src/retur/rc/DetailMain.vue';

// suppiler
import suppRegDetailMain from './src/supp/suppRegistered/DetailMain.vue';
import suppUserEditMain from './src/supp/suppUserEdit/DetailMain.vue';

//csg
import csPfiDetailMain from './src/csg/cspfi/DetailMain.vue';
import csNrDetailMain from './src/csg/csnr/DetailMain.vue';
import csSlaDetailMain from './src/csg/cssla/DetailMain.vue';
import csinvDetailMain from './src/csg/csinv/DetailMain.vue';

// credit note
import cnDetailMain from './src/cn/DetailMain.vue';

import prdDetailMain from './src/prd/DetailMain.vue';
import storeEditMain from './src/adm/store/DetailMain.vue';
import warehouseEditMain from './src/adm/warehouse/DetailMain.vue';
import businessUnitEditMain from './src/adm/businessUnit/DetailMain.vue';
import internalUserEditMain from './src/adm/internalUser/DetailMain.vue';

export const ItemCodeCell = withInstall(itemCodeCell);
export const Status = withInstall(status);
export const BarcodeBankPromoSalesType = withInstall(barcodeBankPromoSalesType);
export const Field = withInstall(field);
export const Remark = withInstall(remark);
export const ItemNameCell = withInstall(itemNameCell);
export const QtyOrderCell = withInstall(quantityOrderCell);
export const DropDocumentItem = withInstall(dropDocumentItem);
export const DropDocumentRetur = withInstall(dropDocumentRetur);
export const DocumentDropdownButton = withInstall(documentDropdownButton);
export const DocumentReturDropdownButton = withInstall(documentReturDropdownButton);
export const PoDetailMain = withInstall(poDetailMain);
export const RaDetailMain = withInstall(raDetailMain);
export const RarDetailSupport = withInstall(rarDetailSupport);
export const RarDetailSupplier = withInstall(rarDetailSupplier);
export const PfiDetailMain = withInstall(pfiDetailMain);
export const PfirDetailSupplier = withInstall(pfirDetailSupplier);
export const InvDetailMain = withInstall(invDetailMain);
export const CNDetailMain = withInstall(cnDetailMain);
export const CSInvDetailMain = withInstall(csinvDetailMain);
export const RRDetailMain = withInstall(rrDetailMain);
export const RRNDetailMain = withInstall(rrnDetailMain);
//export const CpfiDetailMain = withInstall(cpfiDetailMain);
export const RCDetailMain = withInstall(rcDetailMain);
export const RRRDetailMain = withInstall(rrrDetailMain);
export const CSpfiDetailMain = withInstall(csPfiDetailMain);
export const CSnrDetailMain = withInstall(csNrDetailMain);
export const CSslaDetailMain = withInstall(csSlaDetailMain);
export const PrdDetailMain = withInstall(prdDetailMain);
export const StoreEditMain = withInstall(storeEditMain);
export const WarehouseEditMain = withInstall(warehouseEditMain);
export const BusinessUnitEditMain = withInstall(businessUnitEditMain);
export const SuppRegDetailMain = withInstall(suppRegDetailMain);
export const SuppUserEditMain = withInstall(suppUserEditMain);
export const InternalUserEditMain = withInstall(internalUserEditMain);
