interface Option {
  label: string;
  value: string;
  key: string;
}

function createOptions(dropdown: object, sort?: boolean) {
  let DROPDOWN_KEYS = Object.keys(dropdown);
  const options: Option[] = [];

  if (sort == true) {
    DROPDOWN_KEYS = DROPDOWN_KEYS.sort();
  }

  DROPDOWN_KEYS.forEach((element, index) => {
    const option: Option = {
      label: dropdown[element],
      value: element,
      key: index.toString(),
    };
    options.push(option);
  });

  return options;
}

export default createOptions;
