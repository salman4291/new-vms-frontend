import { DescItem } from '/@/components/Description/index';

export const cpfiOrderInfoData = {
  recipientCode: '8990080000000 008 - Logistic',
  deptCode: '11',
  orderNo: '115516001303',
  orderDate: 'Thu, Mar 10,2016, 14:37',
};
export const cpfiOrderInfoSchema: DescItem[] = [
  { field: 'recipientCode', label: 'Kode Penerima' },
  { field: 'deptCode', label: 'Kode Departemen' },
  { field: 'orderNo', label: 'Nomor Order' },
  { field: 'orderDate', label: 'Tanggal Order' },
];

export const cpfiInfoData = {
  pfiDate: 'Tue, Mar 15,2016, 12:38',
  pfiRevision: 0,
};
export const cpfiInfoSchema: DescItem[] = [
  { field: 'pfi_number', label: 'PFI Number' },
  { field: 'tanggal_pfi', label: 'Tanggal PFI' },
  { field: 'revisi', label: 'Revisi' },
  { field: 'store', label: 'Toko' },
  { field: 'department', label: 'Departemen' },
  { field: 'nama_perusahaan', label: 'Nama perusahaan' },
  { field: 'alamat_perusahaan', label: 'Alamat perusahaan' },
  { field: 'npwp', label: 'NPWP' },
  { field: 'sales_start_date', label: 'Sales Start Date' },
  { field: 'sales_end_date', label: 'Sales End Date' },
  { field: 'con_margin_rate', label: 'Consignment Margin Rate' },
  { field: 'con_margin_value', label: 'Consignment Margin Value' },
];

export const supplierData = {
  supplierName: '115516 DWIMITRA MULTIPRATAMA',
  code: '5516',
  telephone: '021-58303642',
  fax: '021-5830342',
};
export const supplierSchema: DescItem[] = [
  {
    field: 'supplier_code',
    label: 'Kode Supplier',
  },
  {
    field: 'supplier_name',
    label: 'Nama Supplier',
  },
  {
    field: 'supplier_phone',
    label: 'Telepon',
  },
  {
    field: 'supplier_fax',
    label: 'Fax',
  },
];

interface Document {
  docKey: string;
  reference: string;
  status: string;
  date: string;
  accepted?: string;
}

export const documents: Document[] = [
  {
    docKey: 'po',
    reference: 'TRI1603238761026',
    status: 'Invoice Ditolak',
    date: 'Fri, Apr 01, 2016, 00:00',
  },
  {
    docKey: 'pfi',
    reference: '9161679',
    status: 'Disahkan',
    date: 'Fri, Apr 01, 2016, 00:00',
  },
  {
    docKey: 'pfi',
    reference: '',
    status: 'Disahkan',
    date: 'Sat, Apr 02, 2016, 00:49',
  },
  {
    docKey: 'inv0',
    reference: '',
    status: 'Ditolak',
    date: 'Fri, Apr 08, 2016, 17:02',
    accepted: '',
  },
  {
    docKey: 'invr',
    reference: '',
    status: 'Dikirim',
    date: 'Mon, Oct 23, 2017, 13:11',
  },
  {
    docKey: 'ir',
    reference: '',
    status: 'Revisi sudah dibuat',
    date: 'Sat, Sep 29, 2018, 10:31',
  },
  {
    docKey: 'inv1',
    reference: '',
    status: 'Draft',
    date: 'Sat, Sep 29, 2018, 10:45',
    accepted: '',
  },
];

export const otherCpfiData = {
  cdt: 'TRI1603238761026',
  totalPrice: '179,618.40',
  additionalInfo: 'Always bring this document when delivering goods.',
};
