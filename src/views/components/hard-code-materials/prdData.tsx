import { DescItem } from '/@/components/Description/index';

export const prdDetailInfoData = {
  recipientCode: '8990080000000 008 - Logistic',
  deptCode: '11',
  orderNo: '115516001303',
  orderDate: 'Thu, Mar 10,2016, 14:37',
};
export const pengirimSchema: DescItem[] = [
  { field: 'recipientCode', label: 'Kode Penerima' },
  { field: 'deptCode', label: 'Kode Departemen' },
  { field: 'orderNo', label: 'Nomor Order' },
  { field: 'orderDate', label: 'Tanggal Order' },
];

export const prdInfoData = {
  pfiDate: 'Tue, Mar 15,2016, 12:38',
  pfiRevision: 0,
};
export const prdInfoSchema: DescItem[] = [
  { field: 'pfiDate', label: 'Tanggal PFI' },
  { field: 'pfiRevision', label: 'Revisi' },
];
