import { DescItem } from '/@/components/Description/index';

export const raOrderInfoData = {
  orderNo: '916021719',
  orderDate: 'Wed, Mar 23,2016, 15:27',
  deptCode: '22',
  requesterGroup: '021',
  deliveryTo: 'Ambassador',
  recipientCode: '1116016047',
};
export const raOrderInfoSchema: DescItem[] = [
  { field: 'po_no', label: 'Nomor Order' },
  { field: 'order_date', label: 'Tanggal Order' },
  { field: 'dept_code', label: 'Kode Departemen' },
  { field: 'store_code', label: 'Kelompok Pemesan' },
  { field: 'delivery_to', label: 'Pengiriman Ke' },
  { field: 'supplier_code', label: 'Kode Penerima' },
];

export const raInfoData = {
  raNo: '1116016047',
  raDate: 'Thu, Mar 24,2016, 00:00',
};
export const raInfoSchema: DescItem[] = [
  { field: 'receiving_advice_number', label: 'Nomor RA' },
  { field: 'receiving_advice_date', label: 'Tanggal RA' },
];

export const supplierData = {
  supplier: 'DOMBA KECIL, CV',
  code: 'N972',
  telephone: '0819-06543268',
  fax: '021-43931145',
};
export const supplierSchema: DescItem[] = [
  {
    field: 'supplier_code',
    label: 'Kode',
  },
  {
    field: 'supplier_phone',
    label: 'Telepon',
  },
  {
    field: 'supplier_fax_number',
    label: 'Fax',
  },
];

interface Document {
  docKey: string;
  reference: string;
  status: string;
  date: string;
  accepted?: string;
}

export const documents: Document[] = [
  {
    docKey: 'po',
    reference: 'TRI1603238761026',
    status: 'Invoice Ditolak',
    date: 'Fri, Apr 01, 2016, 00:00',
  },
  {
    docKey: 'ra',
    reference: '9161679',
    status: 'Disahkan',
    date: 'Fri, Apr 01, 2016, 00:00',
  },
  {
    docKey: 'pfi',
    reference: '',
    status: 'Disahkan',
    date: 'Sat, Apr 02, 2016, 00:49',
  },
  {
    docKey: 'inv0',
    reference: '',
    status: 'Ditolak',
    date: 'Fri, Apr 08, 2016, 17:02',
    accepted: '',
  },
  {
    docKey: 'invr',
    reference: '',
    status: 'Dikirim',
    date: 'Mon, Oct 23, 2017, 13:11',
  },
  {
    docKey: 'ir',
    reference: '',
    status: 'Revisi sudah dibuat',
    date: 'Sat, Sep 29, 2018, 10:31',
  },
  {
    docKey: 'inv1',
    reference: '',
    status: 'Draft',
    date: 'Sat, Sep 29, 2018, 10:45',
    accepted: '',
  },
];

export const otherRaData = {
  supplierDest: 'Domba Kecil, CV',
  cdt: 'TRI1603238761026',
};
